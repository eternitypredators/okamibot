﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Discord;
using Discord.Modules;
using Newtonsoft.Json;

namespace OkamiBot.Services
{
    internal interface IDataModule : IModule
    {
        void OnDataLoad();
    }

    public class DataService : IService
    {
        private class SerializationData
        {
            public IDataModule Module { get; }
            public FieldInfo Field { get; }
            public string SaveDir { get; }

            public SerializationData(IDataModule module, FieldInfo field, string saveDir)
            {
                Module = module;
                Field = field;
                SaveDir = saveDir;
            }
        }

        private DiscordClient _client;

        private const string DataDir = "./data/";

        public void Install(DiscordClient client)
        {
            _client = client;
        }

        private IEnumerable<SerializationData> GetFields<T>() where T : Attribute
        {
            foreach (
                IDataModule module in
                    _client.GetService<ModuleService>()
                        .Modules.Select(m => m.Instance)
                        .Where(m => m.GetType().GetInterfaces().Contains(typeof(IDataModule))))
            {
                Type type = module.GetType();
                IEnumerable<FieldInfo> fields = type.GetRuntimeFields().Where(f => f.GetCustomAttribute<T>() != null);
                foreach (FieldInfo serializeField in fields)
                    yield return
                        new SerializationData(module, serializeField, $"{DataDir}{type.Name}_{serializeField.Name}.json")
                        ;
            }
        }

        public void Load()
        {
            foreach (SerializationData data in GetFields<DataLoadAttribute>())
            {
                try
                {
                    if (!File.Exists(data.SaveDir))
                    {
                        data.Module.OnDataLoad();
                        continue;
                    }
                    string jsondata = File.ReadAllText(data.SaveDir);

                    data.Field.SetValue(
                        data.Module,
                        JsonConvert.DeserializeObject(jsondata, data.Field.FieldType));

                    data.Module.OnDataLoad();
                }
                catch (Exception ex)
                {
                    Program.WriteLog(LogSeverity.Error, $"Failed loading data from field {data.Field.Name}. Exception: {ex}");
                }
            }
        }

        public void Save()
        {
            foreach (SerializationData data in GetFields<DataLoadAttribute>())
            {
                try
                {
                    File.WriteAllText(data.SaveDir,
                        JsonConvert.SerializeObject(data.Field.GetValue(data.Module)));
                }
                catch (Exception ex)
                {
                    Program.WriteLog(LogSeverity.Error, $"Failed loading data from field {data.Field.Name}. Exception: {ex}");
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public abstract class BaseDataAttribute : Attribute
    {
    }

    public class DataLoadAttribute : BaseDataAttribute
    {
    }

    public class DataSaveAttribute : BaseDataAttribute
    {
    }

}
