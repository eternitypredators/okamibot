﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using OkamiBot.Helpers;
using OkamiBot.Modules.TranslatorModule.Helpers;
using System;
using System.Threading.Tasks;

namespace OkamiBot.Modules.TranslatorModule
{
    class TranslatorModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("translate")
                    .Alias("trans")
                    .Description($"Translates from>to text. From the given language to the destiation language. | `-trans en>fr Hello`")
                    .Parameter("langs", ParameterType.Required)
                    .Parameter("text", ParameterType.Unparsed)
                    .Do(TranslateFunc());
                g.CreateCommand("translangs")
                .Description($"List the valid languages for translation. | `-translangs` or `-translangs language`")
                .Parameter("search", ParameterType.Optional)
                .Do(ListLanguagesFunc());
            });

        }
        private GoogleTranslator t = new GoogleTranslator();
        private Func<CommandEventArgs, Task> TranslateFunc() => async e =>
        {
            try
            {
                await e.Channel.SendIsTyping().ConfigureAwait(false);
                string from = e.GetArg("langs").ToLowerInvariant().Split('>')[0];
                string to = e.GetArg("langs").ToLowerInvariant().Split('>')[1];
                var text = e.GetArg("text")?.Trim();
                if (string.IsNullOrWhiteSpace(text))
                    return;

                string translation = await t.Translate(text, from, to).ConfigureAwait(false);
                await e.Channel.SendMessage(translation).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                await e.Channel.SendMessage("Bad input format, or something went wrong...").ConfigureAwait(false);
            }

        };
        private Func<CommandEventArgs, Task> ListLanguagesFunc() => async e =>
        {
            try
            {
                GoogleTranslator.EnsureInitialized();
                string s = e.GetArg("search");
                string ret = "";
                foreach (string key in GoogleTranslator._languageModeMap.Keys)
                {
                    if (!s.Equals(""))
                    {
                        if (key.ToLower().Contains(s))
                        {
                            ret += " " + key + ";";
                        }
                    }
                    else
                    {
                        ret += " " + key + ";";
                    }
                }
                await e.Channel.SendMessage(ret).ConfigureAwait(false);
            }
            catch
            {
                await e.Channel.SendMessage("Bad input format, or something went wrong...").ConfigureAwait(false);
            }
        };
    }
}