﻿using Discord;
using Discord.Audio;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace OkamiBot.Modules
{
    public class AudioModule : IModule
    {
        private DiscordClient _client;
        private Channel currentVoiceChnl;
        private Server currentServer;
        private AudioService _auServ;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            _auServ = _client.GetService<AudioService>();
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("play")
                    .MinPermissions((int)PermissionLevel.User)
                    .Parameter("PATH_OR_URL", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        string nowPlaying = string.Empty;
                        if (currentVoiceChnl != null)
                        {
                            if (e.Message.Text == $"-play {e.GetArg("PATH_OR_URL")}")
                            {
                                SendAudio(@e.GetArg("PATH_OR_URL")); //Does not work yet, but it almost does!...
                                //HtmlWeb web = new HtmlWeb();
                                //HtmlDocument doc = web.Load(e.Args[0]);
                                //if (doc.DocumentNode.SelectSingleNode("//meta[@property='og:site_name']").Attributes["content"].Value == "YouTube")
                                    //nowPlaying = $"Now playing {Format.Bold(doc.GetElementbyId("eow-title").Attributes["title"].Value)} in {Format.Bold(currentVoiceChnl.Name)} voice channel";
                                //await e.Channel.SendMessage(nowPlaying);
                                //_client.Log.Info("Music", nowPlaying + $" @ {e.User.Server.Name}");
                            }
                            else
                            {
                                await _client.ReplyError(e, $"No link was given to be played. Please provide a link.");
                            }
                        }
                        else
                        {
                            await _client.ReplyError(e, $"Unable to play audio due to not being connected to a voice channel.");
                        }   
                    });

                g.CreateCommand("summon")
                    .MinPermissions((int)PermissionLevel.User)
                    .Do(async e =>
                    {
                        var voiceChannel = e.User.VoiceChannel;
                        var server = e.User.Server;
                        if (voiceChannel != null)
                        {
                            currentVoiceChnl = voiceChannel;
                            currentServer = server;
                            var _AudioJoin = _auServ.Join(voiceChannel);
                            await e.Channel.SendMessage($"Joined {Format.Bold(voiceChannel.Name)} voice channel!");
                        }
                        else
                        {
                            await _client.ReplyError(e, $"Unable to connect to voice channel as the requesting user is not in a voice channel.");
                        }
                    });

                g.CreateCommand("disconnect")
                    .MinPermissions((int)PermissionLevel.User)
                    .Do(async e =>
                    {
                        if (currentVoiceChnl != null)
                        {
                            var _vClient = _auServ
                                    .Leave(currentVoiceChnl);
                            await e.Channel.SendMessage($"Disconnected from {Format.Bold(currentVoiceChnl.Name)} voice channel!");
                            currentVoiceChnl = null;
                            currentServer = null;
                        }
                        else
                        {
                            await _client.ReplyError(e, $"Unable to disconnect from voice channel as no channels were connected to.");
                        }
                    });
            });
        }
        public void SendAudio(string pathOrUrl)
        {
            var process = Process.Start(new ProcessStartInfo
            { // FFmpeg requires us to spawn a process and hook into its stdout, so we will create a Process
                FileName = "ffmpeg",
                Arguments = $"-i {pathOrUrl} -f s16le -ar 48000 -ac 2 pipe:1",
                // Here we provide a list of arguments to feed into FFmpeg. -it means the location of the file/URL it will read from
                // Next, we tell it to output 16-bit 48000Hz PCM, over 2 channels, to stdout.
                UseShellExecute = false,
                RedirectStandardOutput = true, // Capture the stdout of the process
                RedirectStandardError = false,
            });
            Thread.Sleep(2000); // Sleep for a few seconds so FFmpeg can start processing data.

            int blockSize = 3840; // The size of bytes to read per frame; 1920 for mono. 3840 for stereo
            byte[] buffer = new byte[blockSize];
            int byteCount;

            while (true) // Loop forever, so data will always be read
            {
                byteCount = process.StandardOutput.BaseStream // Access the underlying MemoryStream from the stdout of FFmpeg
                        .Read(buffer, 0, blockSize); // Read stdout into the buffer

                if (byteCount == 0) // FFmpeg did not output anything
                    break; // Break out of the while(true) loop, since there was nothing to read.

                _auServ.GetClient(currentServer).Send(buffer, 0, byteCount); // Send our data to Discord
            }
            _auServ.GetClient(currentServer).Wait(); // Wait for the Voice Client to finish sending data, as ffMPEG may have already finished buffering out a song, and it is unsafe to return now.
        }
    }
}
