﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OkamiBot.Modules
{
    class ConversionsModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("c2f").Description("Converts Celsius to Fahrenheit")
                    .Parameter("temp")
                    .Do(async e => {
                        if (e.GetArg(0) != null)
                        {
                            var celsVal = double.Parse(e.GetArg("temp"));
                            var fahrVal = (celsVal * (9.0 / 5.0)) + 32;
                            await e.Channel.SendMessage($"{fahrVal}°F");
                        }
                        else
                        {
                            await _client.Reply(e, "You can't do that like this!");
                        }
                    });

                g.CreateCommand("f2c").Description("Converts Fahrenheit to Celsius")
                    .Parameter("temp")
                    .Do(async e => {
                        if (e.GetArg(0) != null)
                        {
                            var fahrVal = double.Parse(e.GetArg("temp"));
                            var celsVal = (fahrVal - 32.0) * (5.0 / 9.0);
                            await e.Channel.SendMessage($"{celsVal}°C");
                        }
                        else
                        {
                            await _client.Reply(e, "You can't do that like this!");
                        }
                    });

                g.CreateCommand("lb2kg").Description("Converts pounds (lb) to kilograms (kg).")
                    .Parameter("Weight-In-Pounds")
                    .Do(async e =>
                    {
                        //Getting pound value from user's input.
                        var poundVar = double.Parse(e.GetArg("Weight-In-Pounds"));
                        //Doing the conversion and storing into a variable.
                        var kiloVar = (poundVar / 2.2046226218);
                        //Sending the result back to the Discord User.
                        await e.Channel.SendMessage($"{poundVar}lb = {kiloVar}kg");
                    });

                g.CreateCommand("kg2lb").Description("Converts kilograms (kg) to pounds (lb).")
                    .Parameter("Weight-In-Kilograms")
                    .Do(async e =>
                    {
                        //Getting kilogram value from user's input.
                        var kiloVar = double.Parse(e.GetArg("Weight-In-Kilograms"));
                        //Doing the conversion and storing into a variable.
                        var poundVar = (kiloVar * 2.2046226218);
                        //Sending the result back to the Discord User.
                        await e.Channel.SendMessage($"{kiloVar}kg = {poundVar}lb");
                    });
            });
        }
    }
}
