﻿using Discord;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Modules;
using Discord.Net;
using OkamiBot.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OkamiBot.Modules.TwitchModule {
	public class TwitchModule : IModule {
		private ModuleManager _manager;
		private DiscordClient _client;
        private bool _isRunning;
		private HttpService _http;
		private SettingsManager<Settings> _settings;

		void IModule.Install(ModuleManager manager) {
			_manager = manager;
			_client = manager.Client;
			_http = _client.GetService<HttpService>();
			_settings = _client.GetService<SettingsService>()
				.AddModule<TwitchModule, Settings>(manager);

			manager.CreateCommands("streams", streams => {

                streams.CreateCommand("enable")
                    .MinPermissions((int)PermissionLevel.ServerAdmin)
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        var settings = _settings.Load(e.Server);
                        if (!settings.TwitchStreamAnnouncmentsEnabled)
                        {
                            settings.TwitchStreamAnnouncmentsEnabled = true;

                            await _settings.Save(e.Server, settings);
                            await e.Channel.SendMessage("Twitch Stream Announcements are now enabled for this server.");
                        }
                        else await _client.Reply(e, $"Twitch Stream Announcments are already enabled for this server.");
                    });

                streams.CreateCommand("disable")
                    .MinPermissions((int)PermissionLevel.ServerAdmin)
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        var settings = _settings.Load(e.Server);
                        if (settings.TwitchStreamAnnouncmentsEnabled)
                        {
                            settings.TwitchStreamAnnouncmentsEnabled = false;

                            await _settings.Save(e.Server, settings);
                            await e.Channel.SendMessage("Twitch Stream Announcements are now disabled for this server.");
                        }
                        else await _client.Reply(e, $"Twitch Stream Announcments are not enabled for this server.");
                    });

                streams.CreateCommand("list")
					.Do(async e => {
						StringBuilder builder = new StringBuilder();
						var settings = _settings.Load(e.Server);
                        if (settings.TwitchStreamAnnouncmentsEnabled)
                        {
                            foreach (var channel in settings.Channels)
                                builder.AppendLine($"{_client.GetChannel(channel.Key)}:\n   {string.Join(", ", channel.Value.Streams.Select(x => x.Key))}");
                            await _client.Reply(e, "Linked Streams", builder.ToString());
                        }
                        else await _client.Reply(e, $"Twitch Stream Announcments are currently disabled for this server. Please contact your server owner/admins if you wish to enable this feature.");
					});

				streams.CreateCommand("add")
                    .MinPermissions((int)PermissionLevel.ChannelModerator)
					.Parameter("twitchuser")
					.Parameter("channel", ParameterType.Optional)
					.Do(async e => {
						var settings = _settings.Load(e.Server);

                        if (settings.TwitchStreamAnnouncmentsEnabled)
                        {
                            Channel channel;
                            if (e.Args[1] != "")
                                channel = await _client.FindChannel(e, e.Args[1], ChannelType.Text);
                            else
                                channel = e.Channel;
                            if (channel == null) return;

                            var channelSettings = settings.GetOrAddChannel(channel.Id);
                            if (channelSettings.AddStream(e.Args[0]))
                            {
                                await _settings.Save(e.Server, settings);
                                await _client.Reply(e, $"Linked stream {e.Args[0]} to {channel.Name}.");
                            }
                            else
                                await _client.Reply(e, $"Stream {e.Args[0]} is already linked to {channel.Name}.");
                        }
                        else await _client.Reply(e, $"Twitch Stream Announcments are currently disabled for this server. Please contact your server owner/admins if you wish to enable this feature.");
                    });

				streams.CreateCommand("remove")
                    .MinPermissions((int)PermissionLevel.ChannelModerator)
					.Parameter("twitchuser")
					.Parameter("channel", ParameterType.Optional)
					.Do(async e => {
						var settings = _settings.Load(e.Server);

                        if (settings.TwitchStreamAnnouncmentsEnabled)
                        {
                            Channel channel;
                            if (e.Args[1] != "")
                                channel = await _client.FindChannel(e, e.Args[1], ChannelType.Text);
                            else
                                channel = e.Channel;
                            if (channel == null) return;

                            var channelSettings = settings.GetOrAddChannel(channel.Id);
                            if (channelSettings.RemoveStream(e.Args[0]))
                            {
                                await _settings.Save(e.Server.Id, settings);
                                await _client.Reply(e, $"Unlinked stream {e.Args[0]} from {channel.Name}.");
                            }
                            else
                                await _client.Reply(e, $"Stream {e.Args[0]} is not currently linked to {channel.Name}.");
                        }
                        else await _client.Reply(e, $"Twitch Stream Announcments are currently disabled for this server. Please contact your server owner/admins if you wish to enable this feature.");
                    });

				streams.CreateGroup("set", set => {
                    set.MinPermissions((int)PermissionLevel.ChannelModerator);
					set.CreateCommand("sticky")
						.Parameter("value")
						.Parameter("channel", ParameterType.Optional)
						.Do(async e => {
                            var settings = _settings.Load(e.Server);
                            if (settings.TwitchStreamAnnouncmentsEnabled)
                            {
                                bool value = false;
                                bool.TryParse(e.Args[0], out value);

                                Channel channel;
                                if (e.Args[1] != "")
                                    channel = await _client.FindChannel(e, e.Args[1], ChannelType.Text);
                                else
                                    channel = e.Channel;
                                if (channel == null) return;

                                var channelSettings = settings.GetOrAddChannel(channel.Id);
                                if (channelSettings.UseSticky && !value && channelSettings.StickyMessageId != null)
                                {
                                    var msg = channel.GetMessage(channelSettings.StickyMessageId.Value);
                                    try { await msg.Delete(); } catch (HttpException ex) when (ex.StatusCode == HttpStatusCode.NotFound) { }
                                }
                                channelSettings.UseSticky = value;
                                await _settings.Save(e.Server, settings);
                                await _client.Reply(e, $"Stream sticky for {channel.Name} set to {value}.");
                            }
                            else await _client.Reply(e, $"Twitch Stream Announcments are currently disabled for this server. Please contact your server owner/admins if you wish to enable this feature.");
                        });
				});
			});

			_client.Ready += (s, e) => {
				if (!_isRunning) {
					Task.Run(Run);
					_isRunning = true;
				}
			};
		}
		public async Task Run() {
			var cancelToken = _client.CancelToken;
			StringBuilder builder = new StringBuilder();

			try {
				while (!_client.CancelToken.IsCancellationRequested) {
                    foreach (var settings in _settings.AllServers)
                    {
                        if (settings.Value.TwitchStreamAnnouncmentsEnabled)
                        {
                            bool isServerUpdated = false;
                            foreach (var channelSettings in settings.Value.Channels)
                            {
                                bool isChannelUpdated = false;
                                var channel = _client.GetChannel(channelSettings.Key);
                                if (channel != null && channel.Server.CurrentUser.GetPermissions(channel).SendMessages)
                                {
                                    foreach (var twitchStream in channelSettings.Value.Streams)
                                    {
                                        try
                                        {
                                            var content = await _http.Send(HttpMethod.Get, $"https://api.twitch.tv/kraken/streams/{Uri.EscapeUriString(twitchStream.Key)}?client_id=6me79qz87niq4r83cyb1w2q59gktp8h");
                                            var response = await content.ReadAsStringAsync();
                                            var json = JsonConvert.DeserializeObject(response) as JToken;

                                            var wasStreaming = twitchStream.Value.IsStreaming;
                                            var lastSeenGame = twitchStream.Value.CurrentGame;

                                            var streamJson = json["stream"];
                                            var isStreaming = streamJson.HasValues;
                                            var currentGame = streamJson.HasValues ? streamJson.Value<string>("game") : null;

                                            if (wasStreaming) //Online
                                            {
                                                if (!isStreaming) //Now offline
                                                {
                                                    _client.Log.Info("Twitch", $"{twitchStream.Key} is no longer streaming.");
                                                    twitchStream.Value.IsStreaming = false;
                                                    twitchStream.Value.CurrentGame = null;
                                                    isChannelUpdated = true;
                                                }
                                                else if (lastSeenGame != currentGame) //Switched game
                                                {
                                                    _client.Log.Info("Twitch", $"{twitchStream.Key} is now streaming {currentGame}.");
                                                    twitchStream.Value.IsStreaming = true;
                                                    twitchStream.Value.CurrentGame = currentGame;
                                                    isChannelUpdated = true;
                                                }
                                            }
                                            else //Offline
                                            {
                                                if (isStreaming) //Now online
                                                {
                                                    _client.Log.Info("Twitch",
                                                        currentGame != null
                                                            ? $"{twitchStream.Key} has started streaming {currentGame}."
                                                            : $"{twitchStream.Key} has started streaming.");
                                                    var nowLive = $"{Format.Bold($"{Format.Escape($"{twitchStream.Key}")}")} is now live! (http://twitch.tv/{twitchStream.Key})";
                                                    if (currentGame != null) nowLive += $"\n{Format.Bold("Playing:")} {currentGame}";
                                                    else nowLive += $"\n{Format.Bold("Playing:")} Unknown/Not Displayed";
                                                    var nowLiveMsg = await channel.SendMessage(nowLive).ConfigureAwait(true);
                                                    if (twitchStream.Value.MessageId != 0) await channel.GetMessage(twitchStream.Value.MessageId).Delete();
                                                    twitchStream.Value.MessageId = nowLiveMsg.Id;
                                                    twitchStream.Value.IsStreaming = true;
                                                    twitchStream.Value.CurrentGame = currentGame;
                                                    isChannelUpdated = true;
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            _client.Log.Error("Twitch", ex);
                                            await Task.Delay(5000);
                                            continue;
                                        }
                                    }
                                } //Stream Loop

                                /*if (channelSettings.Value.UseSticky && (isChannelUpdated || channelSettings.Value.StickyMessageId == null))
                                {
                                    //Build the sticky post
                                    builder.Clear();
                                    builder.AppendLine(Format.Bold("Current Streams:"));
                                    foreach (var stream in channelSettings.Value.Streams)
                                    {
                                        var streamData = stream.Value;
                                        if (streamData.IsStreaming)
                                        {
                                            if (streamData.CurrentGame != null)
                                                builder.AppendLine(Format.Escape($"{stream.Key} - {streamData.CurrentGame} (http://www.twitch.tv/{stream.Key})"));
                                            else
                                                builder.AppendLine(Format.Escape($"{stream.Key} (http://www.twitch.tv/{stream.Key}))"));
                                        }
                                    }
                                    //Edit the old message or make a new one
                                    string text = builder.ToString();
                                    if (channelSettings.Value.StickyMessageId != null)
                                    {
                                        try
                                        {
                                            await _client.StatusAPI.Send(
                                                new UpdateMessageRequest(channelSettings.Key, channelSettings.Value.StickyMessageId.Value) { Content = text });
                                        }
                                        catch (HttpException)
                                        {
                                            _client.Log.Error("Twitch", "Failed to edit message.");
                                            channelSettings.Value.StickyMessageId = null;
                                        }
                                    }
                                    if (channelSettings.Value.StickyMessageId == null)
                                    {
                                        channelSettings.Value.StickyMessageId = (await _client.SendMessage(_client.GetChannel(channelSettings.Key), text)).Id;
                                        isChannelUpdated = true;
                                    }
                                    //Delete all old messages in the sticky'd channel to keep our message at the top
                                    try
                                    {
                                        var msgs = await _client.DownloadMessages(channel, 50);
                                        foreach (var message in msgs
                                                .OrderByDescending(x => x.Timestamp)
                                                .Where(x => x.Id != channelSettings.Value.StickyMessageId)
                                                .Skip(3))
                                            await _client.DeleteMessage(message);
                                    }
                                    catch (HttpException) { }
                                }*/
                                isServerUpdated |= isChannelUpdated;
                            } //Channel Loop
                            if (isServerUpdated)
                                await _settings.Save(settings);
                        }
                    } //Server Loop
					await Task.Delay(1000 * 60, cancelToken); //Wait 60 seconds between full updates
				}
			} catch (TaskCanceledException) { }
		}
	}
}
