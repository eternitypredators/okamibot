﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using OkamiBot.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace OkamiBot.Modules
{
    class MiscellaneousModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("poi")
                    .Description("poi~")
                    .Parameter("poi", ParameterType.Optional)
                    .Hide()
                    .Do(async e =>
                    {
                        await e.Channel.SendTTSMessage("poipoipoipoipoipoipoipoipoipoi~");
                    });

                g.CreateCommand("rekt")
                    .Alias("rekt") //just because I love you. ;)
                    .Description("Shows you how REKT you are. ")
                    .Parameter("rekt", ParameterType.Optional)
                    .Do(async e =>
                    {
                        string msg = "  ☐  Not REKT\n☑ REKT\n☑ Really Rekt\n☑ REKTangle\n☑ SHREKT\n☑ REKT-it Ralph\n☑ Total REKTall\n☑ The Lord of the REKT\n☑ The Usual SusREKTs\n☑ North by NorthREKT\n☑ REKT to the Future\n☑ Once Upon a Time in the REKT\n☑ Full mast erektion\n☑ Rektum\n☑ Resurrekt\n☑ CorRekt\n☑ Indirekt\n☑ Tyrannosaurus Rekt";
                        await e.Channel.SendMessage(msg);
                    });

                g.CreateCommand("r3kt")
                    .Alias("r3kt")
                    .Description("Much rekt")
                    .Parameter("r3kt", ParameterType.Optional)
                    .Do(async e =>
                    {
                        await e.Channel.SendMessage("http://i.imgur.com/HT7rOBu.gif");
                    });

                g.CreateCommand("bestgirl")
                    .Hide()
                    //.Alias("@OkamiBot show me best girl")
                    .Description("Showing love to best girl")
                    .Do(async e =>
                    {
                        //My internet is too slow for this. x.x
                        await e.Channel.SendMessage("I'll show you who's the best girl!");
                        await e.Channel.SendFile(@"C:\Users\Jullian\bestgirl.jpg");
                        await e.Channel.SendMessage("Ara Haan is! ♥♥♥");
                    });

                g.CreateCommand("bestchung")
                    .Hide()
                    .Alias(new string[] { "show me best chung!", "Chung is best!", "Chung is best", "show me best chung" })
                    .Description("Showing love to the best Pikachung!~ ;D ♥")
                    .Do(async e =>
                    {
                        //My internet is too slow for this. x.x
                        await e.Channel.SendMessage("I'll show you who's the best kind of Chung!");
                        await e.Channel.SendFile(@"C:\Users\Jullian\bestchung.jpg");
                        await e.Channel.SendMessage("Pikachungy the Iron Princess! ♥♥♥");
                    });

                /*g.CreateCommand("miho")
                    .Alias(new string[] { "@OkamiBot do you know anything about Miho", "@OkamiBot miho?", "!bestplayerNA" })
                    .Description("The words of the lovely Miho.")
                    .Parameter("miho", ParameterType.Multiple)
                    .Do(async e =>
                    {
                        await e.Channel.SendMessage("Miho: Would you like to some hunting? Huhuhu.");
                    });

                g.CreateCommand("parrot")
                    .Description("caw")
                    .Parameter("parrot", ParameterType.Multiple)
                    .Do(async e =>
                    {
                        await e.Channel.SendMessage("Working on it~");
                    });*/

                g.CreateCommand("hello")
                    .Description("Greet the Okami")
                    .Parameter("OkamiBot", ParameterType.Multiple)
                    .Hide()
                    .Do(async e =>
                    {
                        await _client.Reply(e, "Hmm? What is it you greet me for?\nIf you wish to get my blessing you may call out to me with `-help`.");
                    });

                g.CreateCommand("ud").Alias("urbandictionary")
                    .Parameter("query", ParameterType.Unparsed)
                    .Do(async e => {
                        var baseUri = "http://api.urbandictionary.com/v0/define?term=";
                        if (!string.IsNullOrWhiteSpace(e.GetArg("query")))
                        {
                            var strSource = new System.Net.WebClient().DownloadString($"{baseUri}{e.GetArg("query")}");
                            var results = JsonConvert.DeserializeObject<Results>(strSource);
                            try
                            {
                                await e.Channel.SendMessage($"__**First Urban Dictionary Definition found for {e.GetArg("query")}**__\n{results.List.FirstOrDefault().Definition}\n\nLink: {results.List.FirstOrDefault().PermaLink }");
                            }
                            catch (Exception)
                            {
                                await e.Channel.SendMessage($"No Urban Dictionary Definition found for {Format.Bold($"{e.GetArg("query")}")}.");
                            }
                        }
                    });

                g.CreateCommand("yt").Alias("youtube")
                    .Parameter("query", ParameterType.Unparsed)
                    .Description($"Searches youtubes and shows the first result | `-yt query`")
                    .Do(async e =>
                    {
                        if (!(await SearchHelper.ValidateQuery(e.Channel, e.GetArg("query")).ConfigureAwait(false))) return;
                        var link = await SearchHelper.FindYoutubeUrlByKeywords(e.GetArg("query")).ConfigureAwait(false);
                        if (string.IsNullOrWhiteSpace(link))
                        {
                            await e.Channel.SendMessage("No results found for that query.");
                            return;
                        }
                        var shortUrl = await SearchHelper.ShortenUrl(link).ConfigureAwait(false);
                        await e.Channel.SendMessage(shortUrl).ConfigureAwait(false);
                    });

                g.CreateCommand("safebooru")
                    .Description($"Shows a random image from safebooru with a given tag. Tag is optional but preffered. (multiple tags are appended with +) | `-safebooru yuri+kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";
                        var link = await SearchHelper.GetSafebooruImageLink(tag).ConfigureAwait(false);
                        if (link == null)
                            await e.Channel.SendMessage("`No results.`");
                        else
                            await e.Channel.SendMessage(link).ConfigureAwait(false);
                    });

                g.CreateCommand("test")
                    .Description("Test function, just for grabbing people ♥.")
                    .Alias(new string[] { "grab", "grabRyv", "grabDeri" })
                    .Parameter("user", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        if (e.Message.Text == "-test" || e.Message.Text == "-grab" || e.Message.Text == "-grabRyv" || e.Message.Text == "-grabFluff") e.Args[0] = "97962757831266304";
                        IEnumerable<User> _users = e.Server.FindUsers(e.Args[0]).ToArray();
                            foreach (User u in _users)
                                await e.Channel.SendMessage($"{u.Mention} you baka! :anger:");
                    });

                g.CreateCommand("8ball")
                    .Description("Magic 8 Ball!")
                    .Parameter("Question", ParameterType.Unparsed)
                    .Do(async e=>
                    {
                        if (e.Message.Text == $"-8ball {e.Args[0]}")
                        {
                            var msg = e.Message.Text.Replace("-8ball ", "");
                            var ballResponse = Magic8BallResponse();
                            await _client.Reply(e, $"__**You have shaken the Magic 8 Ball!...**__\n**Question: **{msg}\n**Response: **{ballResponse}");
                        }
                        else await _client.Reply(e, "No signs of a question was asked. Magic 8 Ball refuses to be shaken.");
                    });

                /*cS.CreateGroup("mal", MyAnimeList =>
                {
                    MyAnimeList.CreateCommand("list")
                            .Description("Gives link to MyAnimeList requested user page.")
                            .Parameter("List Type")
                            .Parameter("Username")
                            .Do(async e =>
                            {
                                string url = string.Empty;
                                string msg = string.Empty;
                                if (e.Args[0] == "anime" || e.Args[0] == "manga")
                                {
                                    url = $"http://myanimelist.net/{e.Args[0].ToLower()}list/{e.Args[1]}";
                                    msg = $"{Format.Bold("MyAnimeList User:")} {e.Args[1]}\n";
                                    msg += $"{Format.Bold("List Type:")} {e.Args[0].ToUpper()}\n";
                                    msg += $"{Format.Bold("Page URL:")} {url}";
                                    await e.Channel.SendMessage(msg);
                                }
                                else
                                {
                                    msg = $"List type \"{e.Args[0].ToLower()}\" does not exist.";
                                    await _client.Reply(e, msg);
                                }
                            });
                });*/
            });
        }
        private string Magic8BallResponse()
        {
            Random rnd = new Random();
            switch (rnd.Next(0, 20) + 1)
            {
                case 1:
                    return "It is certain.";
                case 2:
                    return "It is decidedly so.";
                case 3:
                    return "Without a doubt.";
                case 4:
                    return "Yes, definitely.";
                case 5:
                    return "You may rely on it.";
                case 6:
                    return "As I see it, yes.";
                case 7:
                    return "Most likely.";
                case 8:
                    return "Outlook good.";
                case 9:
                    return "Yes.";
                case 10:
                    return "Signs point to yes.";
                case 11:
                    return "Reply hazy try again.";
                case 12:
                    return "Ask again later.";
                case 13:
                    return "Better not tell you now.";
                case 14:
                    return "Cannot predict now.";
                case 15:
                    return "Concentrate and ask again.";
                case 16:
                    return "Don't count on it.";
                case 17:
                    return "My reply is no.";
                case 18:
                    return "My sources say no.";
                case 19:
                    return "Outlook not so good.";
                case 20:
                    return "Very doubtful.";
                default:
                    return "Oops, there was an error that occurred. Please kindly tell the developer of this.";
            }
        }
    }
    public class Result
    {

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("author")]
        public string Author { get; set; }

        [JsonProperty("permalink")]
        public string PermaLink { get; set; }

    }

    public class Results
    {

        [JsonProperty("list")]
        public List<Result> List { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

    }
}
