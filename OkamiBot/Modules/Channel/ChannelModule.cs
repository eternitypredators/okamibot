﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace OkamiBot.Modules
{
    public class ChannelModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("say")
                    .Alias(new string[] { "echo", "e", "s" })
                    .Description("Echoes desired text.")
                    .Parameter("say", ParameterType.Unparsed) //Unparsed uses multiple e.Args, e.Args[0], e.Args[1], ect. I sent you a PM regarding this, pls look over it.
                    .Do(async e =>
                    {
                        //if (e.Message.Text.Substring(5) == "@")
                        //{
                        //    await e.Channel.SendMessage($"{Mention.User(e.User)} pong!");
                        //}
                        //else if (e.Message.Text.Substring(5) != "@")
                        //{
                        await e.Channel.SendMessage(e.Args[0]);
                        //}
                    });

                g.CreateCommand("tts")
                    .Alias(new string[] { "@OkamiBot tts" })
                    .Parameter("tts", ParameterType.Unparsed)
                    .Description("Sends TTS message.")
                    .Hide() //not everyone needs to know about this. ;)
                    .Do(async e =>
                    {
                        await e.Channel.SendTTSMessage(string.Concat(e.Args));
                    });

                g.CreateGroup("channel", ChnlCmnds =>
                {
                    ChnlCmnds.MinPermissions((int)PermissionLevel.ServerModerator);

                    ChnlCmnds.CreateCommand("mute").Description("Mutes channel")
                    .Parameter("channel_name", ParameterType.Unparsed)
                    .Do(async e => {
                        var channel = await _client.FindChannel(e, e.GetArg("channel_name"), ChannelType.Voice);
                        channel.Users.ToList().ForEach(async x => await x.Edit(true));
                        await e.Channel.SendMessage("hOnk :o)");
                    });

                    ChnlCmnds.CreateCommand("unmute").Description("Unmutes channel")
                        .Parameter("channel_name", ParameterType.Unparsed)
                        .Do(async e => {
                            var channel = await _client.FindChannel(e, e.GetArg("channel_name"), ChannelType.Voice);
                            channel.Users.ToList().ForEach(async x => await x.Edit(isDeafened: false));
                            await e.Channel.SendMessage("honk :o)");
                        });

                    ChnlCmnds.CreateCommand("text-create")
                        .Alias(new string[] { "txt-crt" })
                        .Description("Creates a text channel by issued moderator.")
                        .Parameter("text channel", ParameterType.Unparsed)
                        .Do(async e =>
                        {
                            await e.Server.CreateChannel(e.Args[0], ChannelType.Text);
                            await _client.Reply(e, $"Text channel {Format.Bold(e.Args[0])} has been made!");
                        });

                    ChnlCmnds.CreateCommand("delete")
                        .Description("Deletes desired channel format: `!channel delete <chnl type> <chnl name>`")
                        .Parameter("channel type")
                        .Parameter("channel")
                        .Do(async e => {
                            var channel = await _client.FindChannel(e, e.Args[1], e.Args[0]);
                            await channel.Delete();
                            await _client.Reply(e, $"Deleted the {e.Args[0]} channel named {Format.Bold(e.Args[1])}!~ :trumpet:");
                        });
                });

            g.CreateCommand("clear")
                .MinPermissions((int)PermissionLevel.ChannelModerator)
                .Description("Admin command to clean up chat messages, whether specific user or in general.")
                .Parameter("count", ParameterType.Optional)
                .Parameter("user", ParameterType.Optional)
                .Parameter("discriminator", ParameterType.Optional)
                .Do(async e =>
                {
                    if (e.Message.Text == $"-clear {e.Args[0]}")
                    {
                        var count = int.Parse(e.Args[0]) + 1;
                        var msgs = await e.Channel.DownloadMessages(count);
                        foreach (Message m in msgs)
                            await m.Delete();
                        _client.Log.Info("Clear Command", $"{e.User.Name} cleared {msgs.Count()-1} messages in Channel: {e.Message.Channel} @ {e.Message.Server} Server.");
                    }
                    else if (e.Message.Text == $"-clear")
                    {
                        var msgs = await e.Channel.DownloadMessages(2);
                        foreach (Message m in msgs)
                            await m.Delete();
                        _client.Log.Info("Clear Command", $"{e.User.Name} cleared {msgs.Count()-1} messages in Channel: {e.Message.Channel} @ {e.Message.Server} Server.");
                    }
                    else
                    {
                        int count = int.Parse(e.Args[0]);
                        string username = e.Args[1];
                        string discriminator = e.Args[2];
                        User[] users = null;

                        if (username != "")
                        {
                            users = await _client.FindUsers(e, username, discriminator);
                            if (users == null) return;
                        }

                        IEnumerable<Message> msgs;
                        var cachedMsgs = e.Channel.Messages;
                        if (cachedMsgs.Count() < count)
                            msgs = (await e.Channel.DownloadMessages(count));
                        else
                            msgs = e.Channel.Messages.OrderByDescending(x => x.Timestamp).Take(count);

                        if (username != "")
                            msgs = msgs.Where(x => users.Contains(x.User));

                        if (msgs.Any())
                        {
                            foreach (Message m in msgs)
                                await m.Delete();
                            _client.Log.Info("Clear Command", $"{e.User.Name} cleared {msgs.Count()-1} messages in Channel: {e.Message.Channel} @ {e.Message.Server} Server.");
                        }
                        else {
                            await _client.ReplyError(e, $"No messages found.");
                        }
                    }
                });
            });
        }
    }
}
