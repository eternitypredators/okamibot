﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using OkamiBot.Services;
using OkamiBot.Modules.osu_Module.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace OkamiBot.Modules.osu_Module
{
    public class osu_Module : IModule
    {
        private DiscordClient _client;
        private HttpService _http;
        private SettingsManager<Settings> _settings;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            _http = _client.GetService<HttpService>();
            _settings = _client.GetService<SettingsService>()
                .AddModule<osu_Module, Settings>(manager);

            manager.CreateCommands("", g =>
            {
                g.CreateGroup("osu", osuapi =>
                {

                    osuapi.CreateCommand("enable")
                    .MinPermissions((int)PermissionLevel.ServerAdmin)
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        var settings = _settings.Load(e.Server);
                        if (!settings.osuModuleEnabled)
                        {
                            settings.osuModuleEnabled = true;

                            await _settings.Save(e.Server, settings);
                            await e.Channel.SendMessage("osu! Module is now enabled for this server.");
                        }
                        else await _client.Reply(e, $"osu! Module is already enabled for this server.");
                    });

                    osuapi.CreateCommand("disable")
                        .MinPermissions((int)PermissionLevel.ServerAdmin)
                        .Do(async e =>
                        {
                            await e.Message.Delete();
                            var settings = _settings.Load(e.Server);
                            if (settings.osuModuleEnabled)
                            {
                                settings.osuModuleEnabled = false;

                                await _settings.Save(e.Server, settings);
                                await e.Channel.SendMessage($"osu! Module is now disabled for this server.");
                            }
                            else await _client.Reply(e, $"osu! Module is not enabled for this server.");
                        });

                    osuapi.CreateCommand("")
                        .Do(async e =>
                        {
                            await e.Message.Delete();
                            await new osu_API_Tasks().GetUser(_settings, _client, e);
                        });

                    /*osuapi.CreateCommand("clear")
                        .Do(async e =>
                        {
                            await e.Channel.SendMessage($"{msgId}");
                            await e.Channel.GetMessage(msgId).Delete();
                        });*/

                    osuapi.CreateCommand("setuser")
                        .Description("Sets your osu! username to current Discord profile.")
                        .Parameter("osu! user")
                        .Do(async e =>
                        {
                            await e.Message.Delete();
                            await new osu_API_Tasks().SetUser(_settings, _client, e);
                        });

                    osuapi.CreateCommand("stats")
                        .Description("Gets osu! user stats of requested user profile.")
                        .Parameter("osu! user", ParameterType.Optional)
                        .Parameter("osu! mode", ParameterType.Optional)
                        .Do(async e =>
                        {
                            await new osu_API_Tasks().GetUserStats(_settings, _client, e);
                        });

                    osuapi.CreateCommand("skin")
                        .MinPermissions((int)PermissionLevel.BotOwner)
                        .Description("Gives link to requested skin.")
                        .Parameter("osu user", ParameterType.Optional)
                        .Do(async e =>
                        {
                            string reqSkin = string.Empty;
                            if (e.Message.Text != "-osu skin")
                            {
                                string info = File.ReadAllText(@"C:\Users\Jullian\skinslist.txt");
                                string[] infoSplit = info.Split('_', '-');
                                int i = 0;
                                int a = 0;
                                for (i = 0; i <= (infoSplit.Length / 3); i++)
                                {
                                    do
                                    {
                                        a++;
                                        reqSkin += $"\n{Format.Bold("     Creator:")} {infoSplit[a - 1]}\n";
                                        reqSkin += $"{Format.Bold("           Skin:")} {infoSplit[a++]}\n";
                                        reqSkin += $"{Format.Bold("Download:")} {infoSplit[a++]}";
                                        reqSkin = string.Empty;
                                        if (infoSplit[a - 1].ToLower() == e.Args[0].ToLower())
                                        {
                                            reqSkin += $"\n{Format.Bold("     Creator:")} {infoSplit[a - 1]}\n";
                                            reqSkin += $"{Format.Bold("           Skin:")} {infoSplit[a++]}\n";
                                            reqSkin += $"{Format.Bold("Download:")} {infoSplit[a++]}\n";
                                            await _client.Reply(e, reqSkin);
                                            break;
                                        }
                                    } while (infoSplit[a].ToLower() != e.Args[0].ToLower());
                                }
                        /*if (e.Args[0] == "Xishiora") - original placeholder
						{
							reqSkin = $"\n{Format.Bold("     Creator:")} Xishiora\n";
							reqSkin += $"{Format.Bold("           Skin:")} Xishiora's Mashup Skin\n";
							reqSkin += $"{Format.Bold("Download:")} http://puu.sh/lQ3AR.osk {Format.Italics("(Xishiora's Mashup.osk)")} \n";
							reqSkin += $"{Format.Bold("    Preview:")} {Format.Italics("Not Available")}";
						}*/
                            }
                            else if (e.Message.Text == "-osu skin")
                            {
                                string info = File.ReadAllText(@"C:\Users\Jullian\skinslist.txt");
                                string[] infoSplit = info.Split('_', '-');
                                int i = 0;
                                int a = 0;
                                for (i = 0; i <= (infoSplit.Length / 3); i++)
                                {
                                    do
                                    {
                                        a++;
                                        reqSkin += $"\n{Format.Bold("     Creator:")} {infoSplit[a - 1]}\n";
                                        reqSkin += $"{Format.Bold("           Skin:")} {infoSplit[a++]}\n";
                                        reqSkin += $"{Format.Bold("Download:")} {infoSplit[a++]}\n";
                                        if (a == infoSplit.Length)
                                        {
                                            await _client.Reply(e, reqSkin);
                                        }
                                    } while (a <= infoSplit.Length);
                                }
                            }
                        });
                });
            });
        }
    }
}