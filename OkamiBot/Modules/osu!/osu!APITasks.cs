﻿using Discord;
using Discord.Commands;
using OkamiBot.Services;
using osu_api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Extensions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace OkamiBot.Modules.osu_Module.Tasks
{
    public class osu_API_Tasks
    {
        private osuAPI osuAPI = new osuAPI(GlobalSettings.osuApi.Osu_Api_Key);
        public osu_API_Tasks()
        {

        }

        public async Task GetUser(SettingsManager<Settings> _settings, DiscordClient _client, CommandEventArgs e)
        {
            var osuUserId = 0;
            foreach (var settings in _settings.AllServers)
            {
                if (settings.Value.osuModuleEnabled)
                {
                    foreach (var discordUserSettings in settings.Value.DiscordUserIds)
                    {
                        var discordUserId = discordUserSettings.Key;
                        if (discordUserId == e.User.Id)
                        {
                            osuUserId = discordUserSettings.Value.UserId;
                            break;
                        }
                    }
                    if (osuUserId != 0)
                    {
                        //Start calling information for the osu! profile here.
                        var user = osuAPI.GetUser(osuUserId, Mode.osu);
                        var userTopScore = osuAPI.GetUserBest(osuUserId, Mode.osu);

                        await e.Channel.SendMessage($"{e.User.Mention} Displaying your `osu!` profile of `{user.username}`\n" + DisplayOsuUserInformation(user.user_id, user.username, user.pp_rank, user.pp_raw, user.country, user.pp_country_rank,
                                              user.ranked_score, user.accuracy, user.playcount, user.total_score, user.level, user.count_rank_ss,
                                              user.count_rank_s, user.count_rank_a, userTopScore.pp, userTopScore.date));
                        break;
                    }
                    else
                    {
                        await _client.Reply(e, $"You have not linked an `osu!` profile yet, no data will be displayed.");
                        break;
                    }
                }
                else
                {
                    await _client.Reply(e, $"osu! Module is currently not enabled in this server. Please contact your server owner/admins if you would like this feature enabled.");
                    break;
                }
            }
        }

        public async Task SetUser(SettingsManager<Settings> _settings, DiscordClient _client, CommandEventArgs e)
        {
            var settings = _settings.Load(e.Server);
            if (settings.osuModuleEnabled)
            {
                string username = e.Args[0];
                if (e.Message.Text == $"-osu setuser {e.Args[0]}")
                {
                    if (CheckUserExistance(username))
                    {
                        await _client.Reply(e, $"\nosu! User: {Format.Bold(username)} was not found!"
                            + "\nDid you make an error? Please try again.");
                    }
                    else
                    {
                        //Start calling information for the osu! profile here.
                        var user = osuAPI.GetUser(username, Mode.osu);
                        var userTopScore = osuAPI.GetUserBest(username, Mode.osu);

                        //Start Saving Settings
                        User discordUser = e.User;
                        if (discordUser == null) return;

                        var DiscordUserSettings = settings.GetOrAddDiscordUserId(discordUser.Id);
                        DiscordUserSettings.UserId = user.user_id;
                        await _settings.Save(e.Server, settings);
                        await _client.Reply(e, $"`osu!` user has been set to `{user.username}`\n" + DisplayOsuUserInformation(user.user_id, user.username, user.pp_rank, user.pp_raw, user.country, user.pp_country_rank,
                                          user.ranked_score, user.accuracy, user.playcount, user.total_score, user.level, user.count_rank_ss,
                                          user.count_rank_s, user.count_rank_a, userTopScore.pp, userTopScore.date));
                    }
                }
                else if (e.Message.Text == "-osu setuser")
                {
                    await _client.Reply(e, $"Username cannot be blank! Please enter your username! :anger:");
                }
            }
            else await _client.Reply(e, $"osu! Module is currently not enabled in this server. Please contact your server owner/admins if you would like this feature enabled.");
        }

        public async Task GetUserStats(SettingsManager<Settings> _settings, DiscordClient _client, CommandEventArgs e)
        {
            var settings = _settings.Load(e.Server);
            if (settings.osuModuleEnabled)
            {
                string username = e.Args[0];
                if (e.Message.Text == $"-osu stats {e.Args[0]}")
                {
                    if (CheckUserExistance(username))
                    {
                        await _client.Reply(e, $"\nosu! User: {Format.Bold(e.Args[0])} was not found!"
                            + "\nDid you make an error? Please try again.");
                    }
                    else
                    {
                        var user = osuAPI.GetUser(username, Mode.osu);
                        var userTopScore = osuAPI.GetUserBest(username, Mode.osu);
                        await e.Channel.SendMessage(DisplayOsuUserInformation(user.user_id, user.username, user.pp_rank, user.pp_raw, user.country, user.pp_country_rank,
                                          user.ranked_score, user.accuracy, user.playcount, user.total_score, user.level, user.count_rank_ss,
                                          user.count_rank_s, user.count_rank_a, userTopScore.pp, userTopScore.date));
                    }
                }
                else if (e.Message.Text == "-osu stats")
                {
                    await _client.Reply(e, $"Please input a username! :anger:");
                }
            }
            else await _client.Reply(e, $"osu! Module is currently not enabled in this server. Please contact your server owner/admins if you would like this feature enabled.");
        }

        public bool CheckUserExistance(string username)
        {
            var url = string.Empty;
            url = $"https://osu.ppy.sh/u/{username}";
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            return doc.DocumentNode.SelectSingleNode("//head/title").InnerText == "User not found!";
        }
        public string DisplayOsuUserInformation(int user_id, string username, int pp_rank, double pp_raw, string country, int pp_country_rank,
                                                double ranked_score, double accuracy, int playcount, long total_score, double level, int count_rank_ss,
                                                int count_rank_s, int count_rank_a, double topScorePp, DateTime topScoreDate)
        {
            string info = $"\n╒═════════════¤User Info.¤═════════════╕\n\n";
            info += $"     Username: - {username}\n";
            info += $"               - #{pp_rank}\n";
            info += $"               - {pp_raw}pp\n";
            info += $"               - {country} #{pp_country_rank}\n";
            info += $" Ranked Score: {ranked_score.ToString("#,##0")}\n";
            info += $" Hit Accuracy: {accuracy.ToString("#0.00")}%\n";
            info += $"   Play Count: {playcount.ToString("#,##0")}\n";
            info += $"  Total Score: {total_score.ToString("#,##0")}\n";
            info += $"Current Level: {Math.Truncate(level)}\n";
            info += $"        Ranks: - SS: {count_rank_ss}\n";
            info += $"               -  S: {count_rank_s}\n";
            info += $"               -  A: {count_rank_a}\n";
            info += $"    Best Play: {topScorePp}pp on\n";
            info += $"               {topScoreDate.ToString("MMMM")} {topScoreDate.Day.ToOrdinalString()}, {topScoreDate.Year}\n";
            info += $"               @ {topScoreDate.ToString("h:mm:ss tt")}\n";
            //info += $"  Other Ranks - CtB #{ctbUser.pp_rank} - {ctbUser.country} #{ctbUser.pp_country_rank}\n";
            //info += $"              - Taiko #{taikoUser.pp_rank} - {taikoUser.country} #{taikoUser.pp_country_rank}\n";
            //info += $"              - osu!Mania #{maniaUser.pp_rank} - {maniaUser.country} #{maniaUser.pp_country_rank}\n"; //DOESN'T WORK FOR SOME REASON... MY API IS RETARDED?????...
            //info =    "╒═════════════¤User Info.¤═════════════╕\n\n";
            info += "\n╘════════════¤End of Info.¤════════════╛";
            return $"{Format.Bold("Profile:")} https://osu.ppy.sh/u/{user_id} \n{Format.Bold("Avatar:")} https://a.ppy.sh/{user_id} {Format.Code(info)}";
        }
    }
}
