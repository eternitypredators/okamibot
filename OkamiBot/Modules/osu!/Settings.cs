﻿using Newtonsoft.Json;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace OkamiBot.Modules.osu_Module
{
    public class Settings
    {
        public bool osuModuleEnabled { get; set; }
        public class DiscordUserId
        {
            public int UserId { get; set; }
        }

        [JsonIgnore]
        private ConcurrentDictionary<ulong, DiscordUserId> _discordUserIds = new ConcurrentDictionary<ulong, DiscordUserId>();
        public IEnumerable<KeyValuePair<ulong, DiscordUserId>> DiscordUserIds { get { return _discordUserIds; } set { _discordUserIds = new ConcurrentDictionary<ulong, DiscordUserId>(value); } }
        public DiscordUserId GetOrAddDiscordUserId(ulong discordUserId) => _discordUserIds.GetOrAdd(discordUserId, x => new DiscordUserId());
        public void RemoveDiscordUserId(ulong discordUserId)
        {
            DiscordUserId ignored;
            _discordUserIds.TryRemove(discordUserId, out ignored);
        }
    }
}
