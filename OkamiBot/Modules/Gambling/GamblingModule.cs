﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OkamiBot.Modules
{
    class GamblingModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("roll")
                    .Description("Dice roller. Doesn't support 1d20+x yet!")
                    .Parameter("diceAmount", ParameterType.Optional)
                    .Parameter("roll", ParameterType.Optional)
                    .Do(async e =>
                    {
                        //taking from ~roll 1d20
                        //Rolling once, from values between 1 to 20, including 1 and 20
                        try
                        {
                            //Creating new random for dice purposes
                            Random rnd = new Random();

                            //Creating new Regular Expression for dice roll format purposes. 
                            Regex reg = new Regex(@"(\d+)d(\d+)");

                            //Capturing the text to check for the RegEx formatting.
                            string fmt = e.Message.Text;
                            if (fmt == "-roll") //Format for rolling a 100
                            { fmt = "-roll 1d100"; }
                            else if (fmt != $"-roll {e.Args[0]}") //Format for rolling multiple of a specified number of faces.
                            { fmt = $"-roll {e.Args[0]}d{e.Args[1]}"; }
                            else //Format for rolling multiple 100s
                            { fmt = $"-roll {e.Args[0]}d100"; }

                            //Match thing that I forgot what it does.
                            Match m = reg.Match(fmt, 4);
                            switch (m.Success)
                            {
                                case true: //if the match succeeded perform the roll action.
                                    //Setting the Minimum and Maximum random values.
                                    int Min = 1; int Max = int.Parse(m.Groups[2].Value) + 1;

                                    //Doing the rolls.
                                    int[] roll = Enumerable
                                        .Repeat(0, int.Parse(m.Groups[1].Value)) //Repeat to however many times specified in the RegEx formatter
                                        .Select(i => rnd.Next(Min, Max)) //Grab a number between 1 and the Max specified in the RegEx formatter and set it to current index.
                                        .ToArray(); //Store result into current index.

                                    //Sending user the result.
                                    await _client.Reply(e, $"has rolled {Format.Code(string.Join(", ", roll))}");
                                    break;

                                case false: //if the match failed or text had "a blunt" then perform this case
                                    if (e.Message.Text.Contains("a blunt"))
                                        await e.Channel.SendMessage("Proceeding to roll blunt.. :evergreen_tree: :smoking: :evergreen_tree: :smoking:");
                                    break;
                            }
                        }
                        catch (FormatException ex)
                        {
                            await e.Channel.SendMessage($"`Error: {ex.Message}`\nWhy would you even try that?! :anger:"); //If something somehow failed.
                        }
                    });

                //Basic Coin Flip Command using Random, RegEx, Match & Enumerable classes to generate a result then send back the result. 
                g.CreateCommand("coinflip")
                    .Description("Flip a coin. Is it heads or tails?")
                    .Parameter("flipAmount", ParameterType.Optional)
                    .Do(async e =>
                    {
                        try
                        {
                            //Random class to generate a number to determine each result.
                            Random rnd = new Random();
                            //RegEx class to check for formats later on.
                            Regex reg = new Regex(@"(\d+)d(\d+)");
                            //Capture user's command message to determine its RegEx formatting later on.
                            string fmt = e.Message.Text;
                            //For flipping coin once.
                            if (fmt == "-coinflip")
                            { fmt = "-coinflip 1d2"; }
                            //For flipping coin to said number of times.
                            else
                            { fmt = $"-coinflip {e.Args[0]}d2"; }
                            //Checking format match.
                            Match m = reg.Match(fmt, 4);
                            //Checks success of format match.
                            switch (m.Success)
                            {
                                case true: //Do this case when match was a success.
                                    //Presetting the minimum and maximum values for the Random class.
                                    int Min = 1; int Max = int.Parse(m.Groups[2].Value) + 1;
                                    //Generating Coin Flip Results Here
                                    int[] coinFlip = Enumerable
                                        .Repeat(0, int.Parse(m.Groups[1].Value)) //Repeating to the number of Coin Flips specified.
                                        .Select(i => rnd.Next(Min, Max)) //Sending result to current index passthrough via Lambda showing what action is passed in each.
                                        .ToArray(); //Sending all results to an array format.
                                    //Checking if the Coin Flip was multiple coins flipped.
                                    if (coinFlip.Length > 1)
                                    {
                                        //Presetting heads & tails count for checking each toss.
                                        int heads = 0; int tails = 0;
                                        //Checking each toss with a for loop.
                                        for (int i = 0; i < coinFlip.Length; i++)
                                        {
                                            if (coinFlip[i] == 1) heads++; //Heads being 1
                                            else tails++; //Tails being 2 or "Other" if something wrong happens... which I won't bother to check since results are random anyway.
                                        }
                                        //Sending Coin Flip results back to Discord User.
                                        await _client.Reply(e, $"\n__**Your Coin Flip Results Are...**__\n**Heads: **{heads}\n**Tails: **{tails}");
                                    }
                                    else
                                    {
                                        //Initiating String Variable For Coin Flip Result
                                        var coinFlipResult = "";
                                        if (coinFlip[0] == 1) coinFlipResult = "Heads"; //Coin Flip result is "Heads" if result is 1
                                        else coinFlipResult = "Tails"; //Coin Flip result is "Tails" if result is 2 or "Other" which I won't bother to check since results are random anyway.
                                        //Sending Coin Flip results back to Discord User.
                                        await _client.Reply(e, $"You flipped a **{coinFlipResult}**.");
                                    }
                                    break;

                                case false: //Do this case if match was not a success.
                                    if (e.Message.Text.Contains("a blunt"))
                                        await e.Channel.SendMessage("Proceeding to roll blunt.. :evergreen_tree: :smoking: :evergreen_tree: :smoking:");
                                    break;
                            }
                        }
                        catch (FormatException ex)
                        {
                            //If an error somehow occurs send back to the Discord Channel that it was called into.
                            await e.Channel.SendMessage($"`Error: {ex.Message}`\nWhy would you even try that?! :anger:");
                        }
                    });
            });
        }
    }
}
