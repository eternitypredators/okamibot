﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OkamiBot.Modules
{
    public class HelpModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;

            manager.CreateCommands("", g => {
                var cs = _client.GetService<CommandService>();
                g.CreateGroup("help", h => {
                    h.CreateCommand("")
                        .Hide()
                        .Description("Display help description.")
                        .Do(async e => {
                            await e.Channel.SendMessage("This command has contains two commands variances in the form of:" + $"{Format.Code("-help <type>")}\n"
                            + "For command help type: `-help commands`\n"
                            + "For text help type: `-help textformat`");
                            //+ $"\nThe help types for users are: {Format.Code("user")}, {Format.Code("admin")} | Note: The user help is broken not working right now. Sorry! -Xishiora\n\n"
                            //+ $"Other help types: {Format.Code("text")}");
                        });

                    h.CreateCommand("commands")
                        .MinPermissions((int)PermissionLevel.User)
                        .Description("Displays a link to the OkamiBot commands listing Google Docs page.")
                        .Do(async e =>
                        {
                            /*string helpstr = $"Use {Format.Code("-")} before a command's name to call it!\n\n";
                            foreach (var cmd in _client.GetService<CommandService>().AllCommands)
                            {
                                    helpstr += $"{Format.Code(cmd.Text)} | Aliases: ";
                                    if (!cmd.Aliases.Any())
                                    {
                                        helpstr += $"{Format.Code("None")}";
                                    }
                                    else
                                    {
                                        helpstr += $"{Format.Code(string.Join(", ", cmd.Aliases))}";
                                    }
                                    helpstr += $"\n{Format.Bold("Description")}: ";
                                    helpstr += $"\n{Format.Italics(cmd.Description)}\n\n";
                            }
                            while (helpstr.Length > 2000)
                            {
                                await e.User.SendMessage(helpstr.Substring(0, 2000));
                                helpstr = helpstr.Substring(2000);
                            }
                            await e.User.SendMessage(helpstr);
                            await _client.Reply(e, $"you've been sent a DM!");*/
                            await e.Channel.SendMessage($"{Format.Bold("Here is the link to the OkamiBot Command Listing!\n")}https://docs.google.com/document/d/1ee7pdwbWo8Fn9LcobKoNfec6BNL1wj0v_nblkY3s2zI/edit?usp=sharing");
                        });

                    /*h.CreateCommand("admin") Probably Obsolete
                        .MinPermissions((int)PermissionLevel.ChannelModerator)
                        .Description("Displays all commands (Only for server admins)")
                        .Do(async e =>
                        {
                            string helpstr = $"Use {Format.Code("-")} before a command's name to call it!\n\n";
                            foreach (var com in _client.GetService<CommandService>().AllCommands)
                            {
                                if (com.IsHidden)
                                {
                                    helpstr += $"{Format.Code(com.Text)} | Aliases: ";
                                    if (!com.Aliases.Any())
                                    {
                                        helpstr += $"{Format.Code("None")}";
                                    }
                                    else
                                    {
                                        helpstr += $"{Format.Code(string.Join(", ", com.Aliases))}";
                                    }
                                    helpstr += $"\n{Format.Bold("Description")}: ";
                                    helpstr += $"\n{Format.Italics(com.Description)}\n\n";
                                }
                            }
                            while (helpstr.Length > 2000)
                            {
                                await e.User.SendMessage(helpstr.Substring(0, 2000));
                                helpstr = helpstr.Substring(2000);
                            }
                            await e.User.SendMessage(helpstr);
                            await _client.Reply(e, $"you've been sent a DM!");
                        });*/

                    h.CreateCommand("textformat")
                        .Alias("text")
                        .Description("Displays text formatting help from requested user in Direct Messages (DM).")
                        .Do(async e =>
                        {
                            await e.User.SendMessage("__**Discord Text Formatting:**__\n\n"
                                                   + "Bold: \\*\\*Bold\\*\\* => **Bold**\n\n"
                                                   + "Italics: \\*Italics\\* => *Italics* or \\_Italics\\_ => _Italics_\n\n"
                                                   + "Bold Italics: \\*\\*\\*Bold Italics\\*\\*\\* => ***Bold Italics*** or \\_\\*\\*Bold Italics\\*\\*\\_ => _**Bold Italics**_\n\n"
                                                   + "Strikeout: \\~\\~Strikeout\\~\\~ => ~~Strikeout~~\n"
                                                   + "Single-line Code Block: \\`My One Liner Code Block <3\\` => `My One Liner Code Block <3`\n\n"
                                                   + "Multi-line Code Block: _(See Below)_\n"
                                                   + "\\`\\`\\` //start it like this then press enter for a new line (Note: After the 3 backticks you can type a language such as \"HTML\" if showing code.)\n"
                                                   + "```\n"
                                                   + "<text here> //pressing enter/return here will not send the message only create a new line\n"
                                                   + "To end... make a new line and type the three backticks again then press enter to send. (See below)\n"
                                                   + "```\n"
                                                   + "\\`\\`\\` <enter to send here>"
                                                   + "\n\nFor better less crappy explanation visit this page:"
                                                   + "\nhttps://support.discordapp.com/hc/en-us/articles/210298617-Markdown-Text-101-Chat-Formatting-Bold-Italic-Underline-");
                        });
                });
            });
        }
    }
}