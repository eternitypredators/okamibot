﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OkamiBot.Modules
{
    public class OwnerModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("setgame")
                    .MinPermissions((int)PermissionLevel.BotOwner)
                    .Description("Sets game that the got is playing.")
                    .Parameter("game", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        _client.SetGame(e.Args[0]);
                    });

                g.CreateCommand("call")
                    .MinPermissions((int)PermissionLevel.BotOwner)
                    .Alias("conshow")
                    .Description("Shows the console for the bot owner.")
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        await _client.Reply(e, "Showing Console!");
                        OkamiBot.Program.ShowConsoleWindow();
                    });

                g.CreateCommand("dismiss")
                    .MinPermissions((int)PermissionLevel.BotOwner)
                    .Alias("conhide")
                    .Description("Hides the console for the bot owner.")
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        await _client.Reply(e, "Hiding Console!");
                        OkamiBot.Program.HideConsoleWindow();
                    });

                g.CreateCommand("exit")
                    .MinPermissions((int)PermissionLevel.BotOwner)
                    .Description("Shutsdown OkamiBot for the bot owner.")
                    .Alias(new string[] { "shutdown" })
                    .Do(async e =>
                    {
                        await e.Message.Delete();
                        _client.Log.Info("Shutdown", $"{e.User.Name} has executed the shutdown command.");
                        await Task.Delay(1500);
                        await _client.Disconnect();
                    });

                g.CreateCommand("restart")
                    .Description("Restarts OkamiBot for the bot owner.")
                    .Alias(new string[] { "reincarnate" })
                    .Do(async e =>
                    {
                        if (e.User.Id == GlobalSettings.Users.DevId)
                        {
                            //await e.Channel.SendMessage("Reincarnating to a stronger form..."); //YOU HAVEN'T EVEN SEEN MY FINAL FORM.
                            await e.Message.Delete();
                            await Task.Delay(1500);
                            var fileName = Assembly.GetExecutingAssembly().Location;
                            Process.Start(fileName);
                            Environment.Exit(0);
                        }
                        else
                        {
                            //await e.User.SendMessage("Hey! Who said you could command me to be reborn into a new form?! :anger: :anger: :anger:"); //OOOOHHH YOU GOT 'EM MADDDDD
                            await e.User.SendMessage($"Hey! You're not allowed to tell me when to beat up Araragi-kun!!!!!! :anger: :anger: :anger:"); //OOOOHHH YOU STILL GOT 'EM MADDDDD
                        }
                    });
            });
        }
    }
}
