﻿using Discord;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Threading.Tasks;
using System.Globalization;

namespace OkamiBot.Modules.AnimeModule {
	public class Helper
    {
		private static DiscordClient _client = Program._client;
		private static string AniToken;
		private static void AniListToken() {
			try {
				var rClient = new RestClient("http://anilist.co/api");
				rClient.UserAgent = _client.Config.UserAgent;
				var request = new RestRequest("auth/access_token", Method.POST);
				request.AddParameter("grant_type", "client_credentials");
				request.AddParameter("client_id", "xipoison-hhwgu");
				request.AddParameter("client_secret", "Uryk6cJCbs4NtgQlDfOrRZ4UI4");
				var execute = rClient.Execute(request);
				AniToken = JObject.Parse(execute.Content)["access_token"].ToString();
			} catch (Exception ex) {
				_client.Log.Warning("AnilistToken", ex);
			}
		}

		public static async Task<Anime> AnimeSearch(string query) {
			try {
				var rClient = new RestClient("http://anilist.co/api");
				rClient.UserAgent = _client.Config.UserAgent;
				var request = new RestRequest("/auth/access_token", Method.POST);

				AniListToken();

				request = new RestRequest("/anime/search/" + Uri.EscapeUriString(query));
				request.AddParameter("access_token", AniToken);

				var obj = JArray.Parse(rClient.Execute(request).Content)[0];

				request = new RestRequest("anime/" + obj["id"]);
				request.AddParameter("access_token", AniToken);
				return await Task.Run(() => JsonConvert.DeserializeObject<Anime>(rClient.Execute(request).Content));
			} catch (Exception ex) {
				_client.Log.Warning("AnimeSearch", ex);
				return null;
			}
		}
		public static async Task<Manga> MangaSearch(string query) {
			try {
				var rClient = new RestClient("https://anilist.co/api");
				var request = new RestRequest("/auth/access_token", Method.POST);
				AniListToken();
				request = new RestRequest("/manga/search/" + Uri.EscapeUriString(query));
				var obj = JArray.Parse(rClient.Execute(request).Content)[0];
				request = new RestRequest("manga/" + obj["id"]);
				request.AddParameter("access_token", AniToken);
				return await Task.Run(() => JsonConvert.DeserializeObject<Manga>(rClient.Execute(request).Content));
			} catch (Exception ex) {
				_client.Log.Warning("MangaSearch", ex);
				return null;
			}
		}

		public static async Task<bool> ValidateQuery(Channel c, string query) {
			if (string.IsNullOrWhiteSpace(query)) {
				await c.SendMessage(":eggplant:");
				return false;
			}
			return true;
		}
	}

	public class Anime {
		public int id;
		public string airing_status;
		public string title_english;
		public int? total_episodes;
		public string description;
		public string image_url_lge;
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        public override string ToString() {
            try {
                return "**Title: **" + title_english +
            "\n**Status: **" + textInfo.ToTitleCase(airing_status) +
            "\n**Episodes: **" + total_episodes +
            "\n**Link: **http://anilist.co/anime/" + id +
            $"\n**Synopsis:**\n{Format.Escape(description.Replace("<br>", "\n"))}\n\n**Cover Image: **{image_url_lge}";
            } catch (Exception) {
                return "**Title: **" + title_english +
            "\n**Status: **" + airing_status +
            "\n**Episodes: **N/A" +
            "\n**Link: **http://anilist.co/anime/" + id +
            $"\n**Synopsis:**\n{Format.Escape(description.Replace("<br>", "\n"))}\n\n**Cover Image: **{image_url_lge}";
			}
		}
	}
	public class Manga {
		public int id;
		public string publishing_status;
		public string image_url_lge;
		public string title_english;
		public int total_chapters;
		public int total_volumes;
		public string description;
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        public override string ToString() =>
			"**Title: **" + title_english +
			"\n**Status: **" + textInfo.ToTitleCase(publishing_status) +
			"\n**Chapters: **" + total_chapters +
			"\n**Volumes: **" + total_volumes +
			"\n**Link: **http://anilist.co/manga/" + id +
			$"\n**Synopsis: **\n{Format.Escape(description.Substring(0, description.Length > 500 ? 500 : description.Length))}\n**Cover Image: **{image_url_lge}";
	}
}
