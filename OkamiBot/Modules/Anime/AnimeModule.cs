﻿using Discord;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Modules;
using OkamiBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace OkamiBot.Modules.AnimeModule {
	public class AnimeModule: IModule {
		private DiscordClient _client;
		private MessageEventArgs _e;
		private bool _isRunning;
		private SettingsManager<Settings> _settings;

		void IModule.Install(ModuleManager manager) {
			_client = manager.Client;
			_settings = _client.GetService<SettingsService>()
				.AddModule<Anime, Settings>(manager);

			manager.CreateCommands("animesearch", g => {
                g.MinPermissions((int)PermissionLevel.ServerAdmin);

				g.CreateCommand("enable")
					//.Parameter("channel", ParameterType.Optional)
					.Do(async e => {
						var settings = _settings.Load(e.Server);

                        if (settings.SearchEnabled != true)
                        {
                            settings.SearchEnabled = true;

                            await _settings.Save(e.Server, settings);

                            Program.WriteLog(LogSeverity.Info, $"Anime/Manga search has been enabled in {e.Server.Name}");
                            await e.Channel.SendMessage($"Anime/Manga search has been enabled.");
                        }
                        else await e.Channel.SendMessage($"Anime/Manga search is already enabled in this server.");
					});
				g.CreateCommand("disable")
					.Do(async e => {
						var settings = _settings.Load(e.Server);

                        if (settings.SearchEnabled == true)
                        {
                            settings.SearchEnabled = false;
                            await _settings.Save(e.Server, settings);

                            Program.WriteLog(LogSeverity.Info, $"Anime/Manga search has been disabled in {e.Server.Name}");
                            await e.Channel.SendMessage($"Anime/Manga search has been disabled.");
                        }
                        else await _client.ReplyError(e, $"Anime/Manga search is not enabled in this server.");
                    });
			}); 
			_client.MessageReceived += (s, e) => {
				_e = e;
				if (_isRunning)
				Task.Run(Run);
				_isRunning = true;
			};
		}

		private async Task Run() {
            try
            {
                if (!_e.Message.IsAuthor)
                {
                    if (_e.Message.Text.First() == '{' && _e.Message.Text.Last() == '}')
                    {
                        var settings = _settings.Load(_e.User.Server);
                        var matches = Regex.Matches(_e.Message.Text, @"{([^{}]*)}"); //(?<=\{)[^}]*(?=\})
                        if (_e.Channel.IsPrivate || settings.SearchEnabled)
                        {
                            var results = matches.Cast<Match>().Select(x => x.Groups[1]).ToList();
                            foreach (var m in results)
                            {
                                if (!await Helper.ValidateQuery(_e.Channel, m.Value)) continue;
                                var result = await Helper.AnimeSearch(m.Value);
                                if (result != null)
                                {
                                    await _e.Channel.SendMessage(result.ToString());
                                }
                            }
                        }
                        else await _e.Channel.SendMessage($"{_e.User.Mention} Anime/Manga searching is not enabled in this server. If you would like to use the Anime/Manga search for this server please contact the server owner/admins to enable this feature.");
                    }
                }
            }
            catch (Exception ex)
            {
                _client.Log.Error("Weeaboo", ex);
            }
		}
	}
}
