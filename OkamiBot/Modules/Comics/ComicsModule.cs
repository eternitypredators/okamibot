﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace OkamiBot.Modules
{
    class ComicsModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("xkcd")
                    .Description("Displays a random xkcd comic.")
                    .Parameter("number", ParameterType.Optional)
                    .Do(async e =>
                    {
                        string url = string.Empty;
                        if (e.Message.Text == "-xkcd")
                        {
                            Random rnd = new Random();
                            int number = rnd.Next(1, 1683);
                            url = $"http://xkcd.com/{number}";
                        }
                        else if (e.Message.Text == $"-xkcd {e.GetArg("number")}")
                            url = $"http://xkcd.com/{e.GetArg("number")}";
                        //Using HTML Agility Pack To Obtain Image Source
                        HtmlWeb web = new HtmlWeb();
                        HtmlDocument doc = web.Load(url);
                        if (doc.DocumentNode.SelectSingleNode("//head/title").InnerText == "404 - Not Found")
                            await _client.Reply(e, $"The xkcd comic you requested for does not exist. Please try again.");
                        else
                            await e.Channel.SendMessage($"{Format.Bold("Title: ")}{doc.GetElementbyId("ctitle").InnerText}\nhttp:{doc.GetElementbyId("comic").Element("img").Attributes["src"].Value}");
                    });

                g.CreateCommand("explosm")
                    .Description("Displays a random explosm comic.")
                    .Parameter("number", ParameterType.Optional)
                    .Do(async e =>
                    {
                        string url = string.Empty;
                        if (e.Message.Text == "-explosm")
                        {
                            Random rnd = new Random();
                            int number = rnd.Next(1, 4305);
                            url = $"http://explosm.net/comics/{number}/";
                        }
                        else if (e.Message.Text == $"-explosm {e.GetArg("number")}")
                            url = $"http://explosm.net/comics/{e.GetArg("number")}/";
                        //Using HTML Agility Pack To Obtain Image Source
                        HtmlWeb web = new HtmlWeb();
                        HtmlDocument doc = web.Load(url);
                        if (doc.DocumentNode.InnerText == "Could not find comic")
                            await _client.Reply(e, $"The explosm comic you requested for does not exist. Please try again.");
                        else await e.Channel.SendMessage($"http:{doc.GetElementbyId("main-comic").Attributes["src"].Value}");
                    });
            });
        }
    }
}
