﻿using Discord;
using Discord.Audio;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OkamiBot.Modules
{
    public class ServerStatsModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateGroup("", stats =>
                {
                    stats.CreateCommand("roles")
                        .Alias(new string[] { /*"@OkamiBot roles", "@OkamiBot show me roles", "@OkamiBot roll us something", "@OkamiBot roll us,"*/ "servroles" })
                        .Description("Displays roles available in the server of the requesting user.")
                        .Parameter("roles", ParameterType.Optional)
                        .Do(async e =>
                        {
                            await e.Channel.SendMessage($"{Format.Bold(e.Server.Name)} - Available Server Roles\n{Format.Code($"・{string.Join("\n・", e.Server.Roles.Where(x => !x.IsEveryone).OrderByDescending(x => x.Position))}")}");
                        });

                    /*stats.CreateCommand("admins")
                        .Alias(new string[] { /*"@OkamiBot admins!", "@OkamiBot show me the admins", "@OkamiBot admins",*/ /*"admin" })
                        .Description("Displays list of Server & Channel Admins")
                        .Parameter("none", ParameterType.Multiple)
                        .Do(async e =>
                        {
                            string admins = string.Empty;
                            if (e.Server.Id == 97368228304539648)
                            {
                                List<string> sAdmins = new List<string>(); List<string> pAdmins = new List<string>(); List<string> eAdmins = new List<string>();
                                User o = e.Server.GetUser(97367693417517056);
                                admins += $"{o.Name}, ";
                                foreach (User u in e.Server.Users)
                                    foreach (Role r in u.Roles)
                                    {
                                        if (r.Name.Contains("Server Admin")) sAdmins.Add(u.Name);
                                        if (r.Name.Contains("PSO2 Admin")) pAdmins.Add(u.Name);
                                        if (r.Name.Contains("Elsword Admin")) eAdmins.Add(u.Name);
                                    }
                                List<string> pFinal = pAdmins.Except(sAdmins).ToList();
                                List<string> eFinal = eAdmins.Except(sAdmins).ToList();
                                string final = $"__Server owner:**__ \n{Format.Code(o.Name)}\n\n";
                                final += $"__**Server Admins:**__ \n{Format.Code(string.Join(", ", sAdmins))}\n\n";
                                final += $"PSO2 Admins: \n{Format.Code(string.Join(", ", pFinal))}\n\n";
                                final += $"__**Elsword Admins**__: \n{Format.Code(string.Join(", ", eFinal))}\n\n";
                                await e.Channel.SendMessage(final);
                            }
                        });

                    stats.CreateCommand("serveradmins")
                        .Alias(new string[] { /*"@OkamiBot server admins", "@OkamiBot show me the server admins", "@OkamiBot server admins!",*/ /*"s-admin" })
                        .Description("Displays list of Server Admins")
                        .Parameter("serveradmins", ParameterType.Optional)
                        .Do(async e =>
                        {
                            if (e.Server.Id == 97368228304539648)
                            {
                                string serveradmins = $"{e.Server.Owner.Name}"; //Don't need the owner unless you didn't list yourself under admin
                            foreach (User u in e.Server.Users)
                                    foreach (Role r in u.Roles)
                                        if (r.Name.Contains("PSO2 NA Admin")) serveradmins += $", {u.Name}";
                                await e.Channel.SendMessage($"__**Server Admins:**__\n"
                                                            + $"{ Format.Code(serveradmins)}");
                            }
                        });

                    stats.CreateCommand("elsadmins")
                        .Alias(new string[] { /*"@OkamiBot Elsword Admins", "@OkamiBot show me Elsword admins", "@OkamiBot who to elsword with?",*/ /*"e-admins" })
                        .Description("Displays list of Elsword NA Admins")
                        .Parameter("elsadmins", ParameterType.Optional)
                        .Do(async e =>
                        {
                            if (e.Server.Id == 97368228304539648)
                            {
                                string elsadmins = $"{e.Server.Owner.Name}";
                                foreach (User u in e.Server.Users)
                                    foreach (Role r in u.Roles)
                                        if (r.Name.Contains("Elsword NA Admin"))
                                            elsadmins += $", {u.Name}";
                                await e.Channel.SendMessage($"__**Elsword Admins:**__\n"
                                                            + $"{ Format.Code(elsadmins)}");
                            }
                        });

                    stats.CreateCommand("pso2admins")
                        .Alias(new string[] { /*"@OkamiBot PSO2 admins", "@OkamiBot show me PSO2 admins", "@OkamiBot star me", "@OkamiBot give me phantasies",*/ /*"p-admins" })
                        .Description("Displays list of PSO2 Admins")
                        .Parameter("pso2admins", ParameterType.Optional)
                        .Do(async e =>
                        {
                            if (e.Server.Id == 97368228304539648)
                            {
                                string pso2admins = $"{e.Server.Owner.Name}";
                                foreach (User u in e.Server.Users)
                                    foreach (Role r in u.Roles)
                                        if (r.Name.Contains("PSO2 NA Admin"))
                                            pso2admins += $", {u.Name}";
                                await e.Channel.SendMessage($"__**PSO2 Admins**__:\n"
                                                            + $"{Format.Code(pso2admins)}");
                            }
                        });*/

                    stats.CreateCommand("servstats")
                        .Alias(new string[] { "serverstats"/*, "@OkamiBot mstats", "@OkamiBot show me the member stats"*/ })
                        .Description("Displays Statistics of Server of the current requesting user.")
                        .Do(async e =>
                        {
                            int int_online; int int_idle; int int_offline; int _total; string str_online; string str_idle; string str_offline;
                            int_online = e.Server.Users.Where(x => x.Status == UserStatus.Online).Count();
                            int_idle = e.Server.Users.Where(x => x.Status == UserStatus.Idle).Count();
                            int_offline = e.Server.Users.Where(x => x.Status == UserStatus.Offline).Count();
                            _total = int_online + int_idle + int_offline;

                            str_online = $"{string.Join(", ", e.Server.Users.Where(x => x.Status == UserStatus.Online).OrderBy(x => x.Name).Take(10))} [...]";
                            str_idle = $"{string.Join(", ", e.Server.Users.Where(x => x.Status == UserStatus.Idle).OrderBy(x => x.Name).Take(10))} [...]";
                            str_offline = $"{string.Join(", ", e.Server.Users.Where(x => x.Status == UserStatus.Offline).OrderBy(x => x.Name).Take(10))} [...]";

                            string info =
                                    $"╒═══════════════════════════════¤Server Info.¤═══════════════════════════════╕\n";
                            info += $"                Server: {e.Server.Name} // {e.Server.Id}\n";
                            info += $"                   Owner: {e.Server.Owner} ¤ Region: {e.Server.Region.Name}\n";
                            info += $"                          Channels: {e.Server.TextChannels.Count()} Text / {e.Server.VoiceChannels.Count()} Voice\n";
                            info += $"                Online: { int_online } ¤ Idle: { int_idle } ¤ Offline: { int_offline } ¤ Total: { _total }\n";
                            info += $"Members Online:\n { str_online }\n\n";
                            info += $"Members Idle:\n { str_idle }\n\n";
                            info += $"Members Offline:\n { str_offline } \n\n";
                            info += $"╘═══════════════════════════════¤End of Info.¤═══════════════════════════════╛";
                            await e.Channel.SendMessage($"{Format.Code(info)}\n{Format.Bold("Server Icon: ")} {e.Server.IconUrl}");
                        });
                });

                g.CreateCommand("avatar")
                    .Description("Displays avatar of the requested user.")
                    .Alias("avi")
                    .Parameter("username", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        IEnumerable<User> _user = e.Server.FindUsers(e.Args[0]);
                        foreach (User u in _user)
                            await e.Channel.SendMessage($"{Format.Bold($"Displaying {u.Nickname}'s Avatar")}\n{u.AvatarUrl}");
                    });

                g.CreateCommand("userinfo")
                    .Description("Displays information of the requested user.")
                    .Parameter("Username", ParameterType.Required)
                    .Do(async e =>
                    {
                        IEnumerable<User> _user = e.Server.FindUsers(e.Args[0]);
                        foreach (User u in _user)
                        {
                            string roles = string.Join(", ", u.Roles.Where(x => !x.IsEveryone));
                            string info = "╒════════════════════════════════¤User Info.¤════════════════════════════════╕\n\n";
                            info += $"​      ​User : {u.Name}\n";
                            info += $"  Nickname : {u.Nickname}\n";
                            if (roles != string.Empty) info += $"     Roles : {roles}\n";
                            else info += $"     Roles : No Given Roles\n";
                            info += $"   User ID : {u.Id}\n";
                            info += $"   Discrim : {u.Discriminator}\n";
                            if (u.AvatarUrl != null) info += $"    Avatar : {u.AvatarUrl}\n";
                            else info += $"    Avatar : No Avatar Has Been Applied or User Not Registered\n";
                            if (u.CurrentGame != null)
                                info += $"      Game : {u.CurrentGame}\n";
                            else info += $"      Game : None\n";
                            info += $" Join date : {u.JoinedAt} UTC\n";
                            info += $" Last Seen : {u.LastOnlineAt} UTC\n";
                            if (u.LastActivityAt != null)
                                info += $"Last Spoke : {u.LastActivityAt} UTC\n\n";
                            else info += "Last Spoke : No Recent Activity\n\n";
                            info += "╘═══════════════════════════════¤End of Info.¤═══════════════════════════════╛";
                            await e.Channel.SendMessage($"Displaying User Information of {Format.Bold($"{u.Name}#{u.Discriminator}:")}\n{Format.Code(info)}");
                        }
                    });
            });
        }
    }
}
