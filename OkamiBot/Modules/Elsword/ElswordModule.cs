﻿using Discord;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using Discord.Modules;
using System;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;

namespace OkamiBot.Modules
{
    public class ElswordModule : IModule
    {
        private DiscordClient _client;
        public TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        void IModule.Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateCommand("okamisite")
                    .Do(async e =>
                    {
                        await e.User.SendMessage($"{Format.Bold("[MysticalOkami] Guild Website:")} http://moguild.wix.com/mysticalokami");
                    });

                g.CreateCommand("okamiwiki")
                    .Do(async e =>
                    {
                        await e.User.SendMessage($"{Format.Bold("[MysticalOkami] Guild Wikipage:")} http://elwiki.net/w/Guild:MysticalOkami");
                    });

                g.CreateCommand("discordinv")
                    .Do(async e =>
                    {
                        await _client.Reply(e, $"{Format.Bold("Here is the Server Invite you requested!! -")} https://discord.gg/XN4ACpd");
                    });

                g.CreateCommand("base").Alias(new string[] { "base.enh" }).Description("Returns enhanceed values to +0 enhance values.")
                    .Parameter("STATS", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        await ReturnToBaseEnhanceValues(e);
                    });

                g.CreateGroup("enh", enh =>
                {
                    enh.CreateCommand("help")
                        .Description("Used for calculating enhancement levels")
                        .Do(async e =>
                        {
                            if (e.Message.Text == "-enh")
                            {
                                await e.Channel.SendMessage($"This is the {Format.Bold("Elsword")} enhancement calculator command.\n"
                                                               + "The command allows you to determine whether you are trying to calculate armor or weapon.\n"
                                                               + "Aside from the functions of this command, here is how you format the command:\n"
                                                               + $"\nThe general format is: {Format.Code("\n-enh <EQP-TYPE> <ENH-LVL> <STATS> <CURNT-ENH-LVL>")}\n{Format.Bold("Note:")} Do NOT include a + before <ENH-LVL> or <CURNT-ENH-LVL>\n"
                                                               + "\nStats **must** be written in this format as shown below (Command does not need to be written in caps):"
                                                               + $"\nCommand Format for Armors: {Format.Code("-enh AR <ENH-LVL> <ATK-TYPE> <ATK-VAL> <P-DEF> <M-DEF> <HP> <CURNT-ENH-LVL>\n")}"
                                                               + $"E.g, {Format.Code("-enh AR 10 PHYS 2000 300 300 1000\n")}"
                                                               + $"There are two {Format.Bold("ATK-TYPEs")} for armors they are:"
                                                               + "\n·`PHYS`\n·`MAG`"
                                                               + $"\n\nCommand Format for Weapons: {Format.Code("-enh WP <ENH-LVL> <P-ATK> <M-ATK> <HP> <CURNT-ENH-LVL>\n")}"
                                                               + $"E.g, {Format.Code("\n-enh WP 12 5000 5000 1250")}"
                                                               + $"\n\n{Format.Bold("HP Increase Value")} is optional for typing statistics values, you may include it if you wish."
                                                               + $"\nHowever when including {Format.Bold("Current Enhancement Level")} (CURNT-ENH-LVL) you **must** include {Format.Bold("HP Increase Value")}."
                                                               + $"\n{Format.Italics("Including Current Enhancement Levels are optional as well, however may be more useful for getting values you are looking for.")}");
                            }
                            else
                            {
                                await _client.ReplyError(e, "How did you even manage to reach this error?!?!");
                            }
                        });
                    enh.CreateCommand("ar").Alias("armor").Description("Calculates Enhancement Statistics for Armors")
                        .Parameter("EQP-STATS", ParameterType.Unparsed)
                        .Do(async e =>
                        {
                            await DoArmorEnhancementCommand(e);
                        });
                    enh.CreateCommand("wp").Alias(new string[] { "weapon", "wep" }).Description("Calculates Enhancement Statistics for Weapons")
                        .Parameter("EQP-STATS", ParameterType.Unparsed)
                        .Do(async e =>
                        {
                            await DoWeaponEnhancementCommand(e);
                        });
                    });
            });
        }
        public async Task ReturnToBaseEnhanceValues(CommandEventArgs e)
        {
            //Intializing Enhancement Level Variable
            int? currentEnhLvl = null;
            //Initializing Given Stat Variables
            int? pAtk = null; int? mAtk = null; int? pDef = null; int? mDef = null; int? hp = null;
            //Initializing Base Stat Variables
            double? basePAtkVal = null; double? baseMAtkVal = null; double? basePdef = null; double? baseMdef = null; double? baseHp = null;

            string[] stat_args = e.GetArg("STATS").Split(' ');
            string[][] stats = stat_args.Select(x => x.Split('.')).ToArray();
            foreach (var stat in stats)
            {
                switch (stat[0])
                {
                    case "p":
                        var tempP = 0;
                        if (int.TryParse(stat[1], out tempP))
                        {
                            if (tempP >= 0) pAtk = tempP;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        break;
                    case "m":
                        var tempM = 0;
                        if (int.TryParse(stat[1], out tempM))
                        {
                            if (tempM >= 0) mAtk = tempM;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        break;
                    case "pd":
                        var tempPd = 0;
                        if (int.TryParse(stat[1], out tempPd))
                        {
                            if (tempPd >= 0) pDef = tempPd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        break;
                    case "md":
                        var tempMd = 0;
                        if (int.TryParse(stat[1], out tempMd))
                        {
                            if (tempMd >= 0) mDef = tempMd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        break;
                    case "hp":
                        var tempHp = 0;
                        if (int.TryParse(stat[1], out tempHp))
                        {
                            if (tempHp >= 0) hp = tempHp;
                            else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        break;
                    case "ce":
                        var temp = 0;
                        if (int.TryParse(stat[1], out temp))
                        {
                            if (temp > 0 && temp <= 20) currentEnhLvl = temp;
                            else if (temp == 0) { await _client.ReplyError(e, "Cannot give base values for what is already base values!"); return; }
                            else { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        break;
                    default:
                        await _client.ReplyError(e, "An invalid argument was given. Please enter only valid arguments!");
                        return;
                }
            }
            //Declaring/Initializing Base Enhance Information String Variable
            string info = "";
            //Starting base values info chunking
            info += "__**Base (+0) Values for Given Values:**__";
            //if physical attack is given, calculate and store info chunk
            if (pAtk != null)
            {
                basePAtkVal = CalculateEnhancement((int)pAtk, 0, (int)currentEnhLvl);
                info += $"\n**Physical Attack: **{Math.Truncate((double)basePAtkVal)}";
            }
            //if magical attack is given, calculate and store info chunk
            if (mAtk != null)
            {
                baseMAtkVal = CalculateEnhancement((int)mAtk, 0, (int)currentEnhLvl);
                info += $"\n**Magical Attack: **{Math.Truncate((double)baseMAtkVal)}";
            }
            //if physical defense is given, calculate and store info chunk
            if (pDef != null)
            {
                basePdef = CalculateEnhancement((int)pDef, 0, (int)currentEnhLvl);
                info += $"\n**Physical Defense: {Math.Truncate((double)basePdef)}**";
            }
            //if magical defense is given, calculate and store info chunk
            if (mDef != null)
            {
                baseMdef = CalculateEnhancement((int)mDef, 0, (int)currentEnhLvl);
                info += $"\n**Magical Defense: {Math.Truncate((double)baseMdef)}**";
            }
            //if hp increase value is given, calculate and store info chunk
            if (hp != null)
            {
                baseHp = CalculateEnhancement((int)hp, 0, (int)currentEnhLvl);
                info += $"\n**HP Increase: {Math.Truncate((double)baseHp)}**";
            }

            //Sending Info To User
            await _client.Reply(e, info);
        }
        public async Task DoArmorEnhancementCommand(CommandEventArgs e)
        {
            //Initalizing Enhancement Level Variable
            int? enhLvl = null; int currentEnhLvl = 0;
            //Initializing Base Stat Variables
            int? basePAtkVal = null; int? baseMAtkVal = null; int? basePdef = null; int? baseMdef = null; int? baseHp = null;
            //Initializing Enhanced Stat Variables
            double? enhAtkVal = null; double? enhPdef = null; double? enhMdef = null; double? enhHp = null;
            string[] equip_stat_args = e.GetArg("EQP-STATS").Split(' ');
            string[][] equip_stats = equip_stat_args.Select(x => x.Split('.')).ToArray();
            foreach (var stat in equip_stats)
            {
                switch (stat[0])
                {
                    case "e":
                        var temp = 0;
                        if (int.TryParse(stat[1], out temp))
                        {
                            if (temp >= 0) enhLvl = temp;
                            else { await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!"); return; }
                        break;
                    case "p":
                        var tempP = 0;
                        if (int.TryParse(stat[1], out tempP))
                        {
                            if (tempP >= 0) basePAtkVal = tempP;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        break;
                    case "m":
                        var tempM = 0;
                        if (int.TryParse(stat[1], out tempM))
                        {
                            if (tempM >= 0) baseMAtkVal = tempM;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        break;
                    case "pd":
                        var tempPd = 0;
                        if (int.TryParse(stat[1], out tempPd))
                        {   
                            if (tempPd >= 0) basePdef = tempPd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        break;
                    case "md":
                        var tempMd = 0;
                        if (int.TryParse(stat[1], out tempMd))
                        {
                            if (tempMd >= 0) baseMdef = tempMd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        break;
                    case "hp":
                        var tempHp = 0;
                        if (int.TryParse(stat[1], out tempHp))
                        {
                            if (tempHp >= 0) baseHp = tempHp;
                            else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        break;
                    case "ce":
                        if (int.TryParse(stat[1], out currentEnhLvl))
                        {
                            if (currentEnhLvl < 0)
                            { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        break;
                    default:
                        await _client.ReplyError(e, "An invalid argument was given. Please enter only valid arguments!");
                        return;
                }
            }
            if (enhLvl == null)
            {
                await _client.ReplyError(e, "No enhancement level was given, please provide an enhance level!");
                return;
            }
            else if (enhLvl == 0 && currentEnhLvl == 0)
            {
                await _client.Reply(e, "There is no reason you would attempt to enhance to +0, as that is the base value. Please give a real enhance level.");
            }
            else if (enhLvl < 0 || enhLvl > 20)
            {
                await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!");
                return;
            }

            //Declaring/Initializing Enhancement Information String Variable
            string info = "";
            //Pre-check for whether use provided both attack values for w/e reason... armors do not give both at this moment.
            if (basePAtkVal != null && baseMAtkVal != null)
            {
                await _client.ReplyError(e, "Please provide only one type of attack stat!");
                return;
            }
            //Checking if Current Enhance Level is Base or not.
            if (currentEnhLvl == 0)
                info += "\n__**Base Armor Values Given**__";
            else
                info += $"\n__**Your +{currentEnhLvl} Armor Values Given**__";
            //Start info chunking the given values into the string.
            if (basePAtkVal != null && baseMAtkVal == null) info += $"\n**Physical Attack: **{basePAtkVal}";
            else if (baseMAtkVal != null && basePAtkVal == null) info += $"\n**Magical Attack: **{baseMAtkVal}";
            if (basePdef != null) info += $"\n**Physical Defense: **{basePdef}";
            if (baseMdef != null) info += $"\n**Magical Defense: **{baseMdef}";
            if (baseHp != null) info += $"\n**HP Increase: **{baseHp}";

            //Starting section for info chunking enhanced values
            info += $"\n\n__**+{enhLvl} Enhancement Values From Given: **__";
            
            //Checking whether base attack is physical or magical attack then calculating enhanced attack and storing info chunk.
            if (basePAtkVal != null && baseMAtkVal == null)
            {
                enhAtkVal = CalculateEnhancement((int)basePAtkVal, (int)enhLvl, currentEnhLvl);
                info += $"\n**Physical Attack: **{Math.Truncate((double)enhAtkVal)}";
            }
            else if (baseMAtkVal != null && basePAtkVal == null)
            {
                enhAtkVal = CalculateEnhancement((int)baseMAtkVal, (int)enhLvl, currentEnhLvl);
                info += $"\n**Magical Attack: **{Math.Truncate((double)enhAtkVal)}";
            }

            //If base physical defense was given, calculate and store into chunk
            if (basePdef != null)
            {
                enhPdef = CalculateEnhancement((int)basePdef, (int)enhLvl, currentEnhLvl);
                info += $"\n**Physical Defense: **{Math.Truncate((double)enhPdef)}";
            }

            //If base magical defense was given, calculate and store info chunk
            if (baseMdef != null)
            {
                enhMdef = CalculateEnhancement((int)baseMdef, (int)enhLvl, currentEnhLvl);
                info += $"\n**Magical Defense: **{Math.Truncate((double)enhMdef)}";
            }

            //If base health points increase was given, calculate and store into chunk.
            if (baseHp != null)
            {
                enhHp = CalculateEnhancement((int)baseHp, (int)enhLvl, currentEnhLvl);
                info += $"\n**HP Increase: **{Math.Truncate((double)enhHp)}";
            }

            //Sending Enhancement Values Back To User.
            await _client.Reply(e, info);
        }

        public async Task DoWeaponEnhancementCommand(CommandEventArgs e)
        {
            //Initalizing Enhancement Level Variable
            int? enhLvl = null; int currentEnhLvl = 0;
            //Initializing Base Stat Variables
            int? basePAtkVal = null; int? baseMAtkVal = null; int? basePdef = null; int? baseMdef = null; int? baseHp = null;
            //Initializing Enhanced Stat Variables
            double? enhPAtkVal = null; double? enhMAtkVal = null; double? enhPdef = null; double? enhMdef = null; double? enhHp = null;
            string[] equip_stat_args = e.GetArg("EQP-STATS").Split(' ');
            string[][] equip_stats = equip_stat_args.Select(x => x.Split('.')).ToArray();
            foreach (var stat in equip_stats)
            {
                switch (stat[0])
                {
                    case "e":
                        var temp = 0;
                        if (int.TryParse(stat[1], out temp))
                        {
                            if (temp >= 0) enhLvl = temp;
                            else { await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!"); return; }
                        break;
                    case "p":
                        var tempP = 0;
                        if (int.TryParse(stat[1], out tempP))
                        {
                            if (tempP >= 0) basePAtkVal = tempP;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Attack!"); return; }
                        break;
                    case "m":
                        var tempM = 0;
                        if (int.TryParse(stat[1], out tempM))
                        {
                            if (tempM >= 0) baseMAtkVal = tempM;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Attack!"); return; }
                        break;
                    case "pd":
                        var tempPd = 0;
                        if (int.TryParse(stat[1], out tempPd))
                        {
                            if (tempPd >= 0) basePdef = tempPd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Physical Defense!"); return; }
                        break;
                    case "md":
                        var tempMd = 0;
                        if (int.TryParse(stat[1], out tempMd))
                        {
                            if (tempMd >= 0) baseMdef = tempMd;
                            else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Magical Defense!"); return; }
                        break;
                    case "hp":
                        var tempHp = 0;
                        if (int.TryParse(stat[1], out tempHp))
                        {
                            if (tempHp >= 0) baseHp = tempHp;
                            else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Health Points!"); return; }
                        break;
                    case "ce":
                        if (int.TryParse(stat[1], out currentEnhLvl))
                        {
                            if (currentEnhLvl < 0)
                            { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        }
                        else { await _client.ReplyError(e, "Invalid Value Given for Current Enhance Level!"); return; }
                        break;
                    default:
                        await _client.ReplyError(e, "An invalid argument was given. Please enter only valid arguments!");
                        return;
                }
            }
            if (enhLvl == null)
            {
                await _client.ReplyError(e, "No enhancement level was given, please provide an enhance level!");
                return;
            }
            else if (enhLvl == 0 && currentEnhLvl == 0) //Checks if user is trying to enhance to base values when the current enhancement is already the base...
            {
                await _client.Reply(e, "There is no reason you would attempt to enhance to +0, as that is the base value. Please give an actual enhance level!"); //Scream at them!
            }
            else if (enhLvl < 0 || enhLvl > 20)
            {
                await _client.ReplyError(e, "Invalid Value Given for Enhancement Level!");
                return;
            }

            //Declaring/Initializing Enhancement Information String Variable
            string info = "";
            //Checking if Current Enhance Level is Base or not.
            if (currentEnhLvl == 0)
                info += "\n__**Base Weapon Values Given**__";
            else
                info += $"\n__**Your +{currentEnhLvl} Weapon Values Given**__";
            //Start info chunking the given values into the string.
            if (basePAtkVal != null) info += $"\n**Physical Attack: **{basePAtkVal}";
            if (baseMAtkVal != null) info += $"\n**Magical Attack: **{baseMAtkVal}";
            if (basePdef != null) info += $"\n**Physical Defense: **{basePdef}";
            if (baseMdef != null) info += $"\n**Magical Defense: **{baseMdef}";
            if (baseHp != null) info += $"\n**HP Increase: **{baseHp}";
            
            //Starting section for info chunking enhanced values.
            info += $"\n\n__**+{enhLvl} Enhancement Values From Given: **__";

            //If base physical attack was given, calculate and store info chunk.
            if (basePAtkVal != null)
            {
                enhPAtkVal = CalculateEnhancement((int)basePAtkVal, (int)enhLvl, currentEnhLvl);
                info += $"\n**Physical Attack: **{Math.Truncate((double)enhPAtkVal)}";
            }

            //If base magical attack was given, calculate and store info chunk.
            if (baseMAtkVal != null)
            {
                enhMAtkVal = CalculateEnhancement((int)baseMAtkVal, (int)enhLvl, currentEnhLvl);
                info += $"\n**Magical Attack: **{Math.Truncate((double)enhMAtkVal)}";
            }

            //If base physical defense was given, calculate and store info chunk
            if (basePdef != null)
            {
                enhPdef = CalculateEnhancement((int)basePdef, (int)enhLvl, currentEnhLvl);
                info += $"\n**Physical Defense: **{Math.Truncate((double)enhPdef)}";
            }

            //If base magical defense was given, calculate and store info chunk
            if (baseMdef != null)
            {
                enhMdef = CalculateEnhancement((int)baseMdef, (int)enhLvl, currentEnhLvl);
                info += $"\n**Magical Defense: **{Math.Truncate((double)enhMdef)}";
            }

            //If base health points increase was given, calculate and store info chunk.
            if (baseHp != null)
            {
                enhHp = CalculateEnhancement((int)baseHp, (int)enhLvl, currentEnhLvl);
                info += $"\n**HP Increase: **{Math.Truncate((double)enhHp)}";
            }

            //Sending Enhancement Information Chunk Back To User.
            await _client.Reply(e, info);
        }

        //Used for Elsword Enhancement Calculator Commands
        public static double? CalculateEnhancement(int baseStat, int enhLvl, int currentEnhLvl = 0)
        {
            var valueIncreaseMultiplier = EnhancementLevelMultiplier(enhLvl);
            if (enhLvl > 0 && currentEnhLvl == 0)
            {
                return baseStat * valueIncreaseMultiplier;
            }
            else if (enhLvl > 0 && currentEnhLvl > 0)
            {
                return (baseStat / EnhancementLevelMultiplier(currentEnhLvl)) * valueIncreaseMultiplier;
            }
            else if (enhLvl == 0 && currentEnhLvl > 0)
            {
                return baseStat / EnhancementLevelMultiplier(currentEnhLvl);
            }
            else return null;
        }
        //Used for above method.
        public static double EnhancementLevelMultiplier(int enhLvl)
        {
            switch (enhLvl)
            {
                case 1:
                    return 1.03;
                case 2:
                    return 1.06;
                case 3:
                    return 1.09;
                case 4:
                    return 1.16;
                case 5:
                    return 1.23;
                case 6:
                    return 1.30;
                case 7:
                    return 1.45;
                case 8:
                    return 1.60;
                case 9:
                    return 1.75;
                case 10:
                    return 2.15;
                case 11:
                    return 2.55;
                case 12:
                    return 2.95;
                case 13:
                    return 3.35;
                case 14:
                    return 3.75;
                case 15:
                    return 4.25;
                case 16:
                    return 4.75;
                case 17:
                    return 5.25;
                case 18:
                    return 5.75;
                case 19:
                    return 6.75;
                case 20:
                    return 8.75;
                default:
                    return 0.00;
            }
        }
    }
}
