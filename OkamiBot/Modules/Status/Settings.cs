﻿using System;

namespace OkamiBot.Modules.StatusModule {
	public class Settings
    {
		public DateTimeOffset LastUpdate { get; set; } = DateTimeOffset.UtcNow;
		public ulong? Channel { get; set; }
	}
}
