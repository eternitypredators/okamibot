﻿using Discord;
using Discord.Commands.Permissions.Levels;
using Discord.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OkamiBot.Modules.ColourModule {
	public class ColourModule: IModule {
		private DiscordClient _client;
		private const string ColorRoleName = "Color";

		void IModule.Install(ModuleManager manager) {
			_client = manager.Client;

			manager.CreateCommands("color", group => {
				group.CreateCommand("set").Hide()
					.Description("Sets your username to a hex color. Format: RRGGBB")
					.Parameter("hex")
					.Do(async e => {
						string stringhex = e.GetArg("hex").ToUpper();
						Role role = e.Server.Roles.FirstOrDefault(x => x.Name == ColorRoleName + stringhex);

						if (role == null || !e.User.HasRole(role) && role.CanEdit()) {
							role = await e.Server.CreateRole(ColorRoleName + stringhex);
                            await role.SetColor(stringhex);
							await e.User.Edit(roles: GetOtherRoles(e.User).Concat(new[] { role }));
						}
						await CleanColorRoles(e.Server);
					});
				group.CreateCommand("clear").Hide()
					.Description("Removes your username color, returning it to default.")
					.Do(async e => {
						await e.User.Edit(roles: GetOtherRoles(e.User));
					});

				group.CreateCommand("clean").Hide()
					.MinPermissions((int)PermissionLevel.ServerModerator)
					.Description("Removes unused color roles. Gets automatically called whenever a color is set.")
					.Do(async e => {
						await CleanColorRoles(e.Server);
					});
			});

		}

		private async Task CleanColorRoles(Server server) {
			foreach (Role role in server.Roles
				.Where(role => role.Name.StartsWith(ColorRoleName))
				.Where(role => !role.Members.Any())) {
				await role.Delete();
			}
		}

		private IEnumerable<Role> GetOtherRoles(User user) => user.Roles.Where(x => !x.Name.StartsWith(ColorRoleName));
	}
}
