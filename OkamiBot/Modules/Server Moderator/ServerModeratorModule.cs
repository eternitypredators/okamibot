﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OkamiBot.Modules
{
    public class ServerModeratorModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateGroup("user", ServerModCmnds =>
                {
                    ServerModCmnds.MinPermissions((int)PermissionLevel.ServerModerator);

                    ServerModCmnds.CreateCommand("mute").Description("Mutes desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await user.Edit(isMuted: true);
                            await e.Channel.SendMessage($"Muted {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("unmute").Description("Unmutes desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await user.Edit(isMuted: false);
                            await e.Channel.SendMessage($"Unmuted {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("deafen").Description("Deafens desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await user.Edit(isDeafened: true);
                            await e.Channel.SendMessage($"Deafened {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("undeafen").Description("Undeafens desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await user.Edit(isDeafened: false);
                            await e.Channel.SendMessage($"Undeafened {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("kick").Description("Kicks desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await user.Kick();
                            await e.Channel.SendMessage($"Kicked {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("ban").Description("Bans desired user.")
                        .Parameter("user").Parameter("discriminator", ParameterType.Optional)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), e.GetArg("discriminator"));
                            await e.Server.Ban(user, 0);
                            await e.Channel.SendMessage($"Banned {Format.Code(user.Name)}. :eggplant:");
                        });

                    ServerModCmnds.CreateCommand("setrole").Description("Sets the desired role onto desired user.")
                        .Parameter("user").Parameter("role")
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), "");
                            if (user == null) await _client.ReplyError(e, "Invalid user.");

                            var role = e.Server.FindRoles(e.GetArg("role")).FirstOrDefault();
                            if (role == null) await e.Channel.SendMessage("Invalid role");

                            try
                            {
                                await user.Edit(roles: new[] { role });
                                await e.Channel.SendMessage($"{Format.Bold(user.Name)} is now in the {Format.Bold(role.Name)} role. :eggplant:");
                            }
                            catch (InvalidOperationException)
                            {
                            }
                            catch (Exception ex)
                            {
                                await e.Channel.SendMessage("I think you made an error!");
                                _client.Log(LogSeverity.Error, $"{ex.GetBaseException().Message}", ex);
                            }
                        });

                    ServerModCmnds.CreateCommand("move").Description("Moves desired user to another voice channel.")
                        .Parameter("user").Parameter("channel_name", ParameterType.Unparsed)
                        .Do(async e => {
                            var user = await _client.FindUser(e, e.GetArg("user"), "");
                            var channel = await _client.FindChannel(e, e.GetArg("channel_name"), ChannelType.Voice);
                            await user.Edit(voiceChannel: channel);
                            if (e.User.Equals(e.Server.Owner))
                                await e.Channel.SendMessage($"{Format.Code(e.User.Name)} has moved {Format.Code(user.Name)} to {Format.Code(channel.Name)}");
                        });
                });
            });
        }
    }
}
