﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Diagnostics;

namespace OkamiBot.Modules
{
    public class BotCommandModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                g.CreateGroup("", invites => {
                    invites.MinPermissions((int)PermissionLevel.BotOwner);
                    invites.CreateCommand("join").Parameter("invite_url").Do(async e => {
                        var invite = await _client.GetInvite(e.GetArg("invite_url"));
                        if (invite.IsRevoked)
                            await e.Channel.SendMessage($"This invite has expired or {_client.Config.UserAgent} has been banned.");
                        await invite.Accept();
                        var msgs = await e.Channel.DownloadMessages(1);
                        foreach (Message m in msgs)
                            await m.Delete();
                        await e.Channel.SendMessage($"Joined the {Format.Bold(invite.Server.Name)} server.");
                        });

                    invites.CreateCommand("botlink")
                        .MinPermissions((int)PermissionLevel.BotOwner).Do(async e =>
                        {
                            await e.Channel.SendMessage($"Hi this is my, {Format.Bold($"{_client.Config.AppName}")}'s link to join servers!"
                                + $"\n{_client.Config.AppUrl}");
                        });
                });

                g.CreateCommand("uptime")
                        .Description($"Displays how long {_client.Config.AppName} has been running for.")
                        .Do(async e => {
                            var info = DateTime.Now - Process.GetCurrentProcess().StartTime;
                            var msg = $"{info.Days} days {info.Hours} hours {info.Minutes} minutes and {info.Seconds} seconds ({info.TotalSeconds.ToString("0.000")}s)";
                            await e.Channel.SendMessage($"{Format.Bold($"{_client.Config.AppName} has been running for:")}{Format.Code($"\n{msg}.")}");
                        });

                g.CreateCommand("botversion")
                    .Alias(new string[] { /*"@OkamiBot v", "@OkamiBot version", */"botver", "botv", "version", "ver"/*, "@OkamiBot Form?"*/ })
                    .Description($"Displays the current version {_client.Config.AppName} is on.")
                    .Do(async e =>
                    {
                        var v = Assembly.GetExecutingAssembly().GetName().Version;
                        await e.Channel.SendMessage($"{_client.Config.AppName} is currently on version `{v.Major}.{v.Minor}.{v.Build}.{v.Revision} (Revision: 10.13.2016)`!");
                    });

                g.CreateCommand("botinfo")
                    .Alias(new string[] { "info" })
                    .Description("Displays Bot's Version and Creator Information.")
                    .Do(async e =>
                    {
                        var v = Assembly.GetExecutingAssembly().GetName().Version;
                        await e.Channel.SendMessage($"__**{_client.Config.AppName} Information**__"
                                                  + $"\n- {Format.Bold("Bot Name: ")}{_client.Config.AppName}"
                                                  + $"\n- {Format.Bold("Author: ")}{GlobalSettings.Users.Name}#{Format.Code(GlobalSettings.Users.Tag.ToString())} (Id:{Format.Code(GlobalSettings.Users.DevId.ToString())})"
                                                  + $"\n- {Format.Bold("Version: ")}`{v.Major}.{v.Minor}.{v.Build}.{v.Revision} (Revision: 10.13.2016)`"
                                                  + $"\n- {Format.Bold("Library: ")}{DiscordConfig.LibName} ({DiscordConfig.LibVersion})"
                                                  + $"\n- {Format.Bold("Runtime: ")}{GetRuntime()} {GetBitness()}"
                                                  + $"\n- {Format.Bold("Uptime: ")}{GetUptime()}"
                                                  + $"\n- {Format.Bold("Bot Join Link: ")}{_client.Config.AppUrl}"
                                                  + $"\n\n__**{_client.Config.AppName} Stats**__"
                                                  + $"\n- {Format.Bold("Heap Size: ")}{GetHeapSize()} MB"
                                                  + $"\n- {Format.Bold("Servers: ")} {_client.Servers.Count()}"
                                                  + $"\n- {Format.Bold("Channels: ")} {_client.Servers.Sum(x => x.AllChannels.Count())}"
                                                  + $"\n- {Format.Bold("Users: ")} {_client.Servers.Sum(x => x.Users.Count())}");
                    });
            });
        }
        private string GetRuntime()
#if NET11
            => ".Net Framework 1.1";
#elif NET20
            => ".Net Framework 2.0";
#elif NET35
            => ".Net Framework 3.5";
#elif NET40
            => ".Net Framework 4.0";
#elif NET45
            => ".Net Framework 4.5";
#elif NET451
            => ".Net Framework 4.5.1";
#elif NET452
            => ".Net Framework 4.5.2";
#elif NET46
            => ".Net Framework 4.6";
#elif NET461
            => ".Net Framework 4.6.1";
#elif NETCORE50
            => ".Net Core 5.0";
#elif DNX451
            => "DNX (.Net Framework 4.5.1)";
#elif DNX452
            => "DNX (.Net Framework 4.5.2)";
#elif DNX46
            => "DNX (.Net Framework 4.6)";
#elif DNX461
            => "DNX (.Net Framework 4.6.1)";
#elif DNXCORE50
            => "DNX (.Net Core 5.0)";
#elif DOTNET50 || NETPLATFORM10
            => ".Net Platform Standard 1.0";
#elif DOTNET51 || NETPLATFORM11
            => ".Net Platform Standard 1.1";
#elif DOTNET52 || NETPLATFORM12
            => ".Net Platform Standard 1.2";
#elif DOTNET53 || NETPLATFORM13
            => ".Net Platform Standard 1.3";
#elif DOTNET54 || NETPLATFORM14
            => ".Net Platform Standard 1.4";
#else
            => "Unknown";
#endif
        private static string GetBitness() => $"{IntPtr.Size * 8}-bit";
        private static string GetUptime() => (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString(@"dd\.hh\:mm\:ss");
        private static string GetHeapSize() => Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString();
    }
}
