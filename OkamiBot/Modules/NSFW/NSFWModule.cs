﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using OkamiBot.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OkamiBot.Modules.NSFWModule
{
    public class NSFWModule : IModule
    {
        private readonly Random rng = new Random();
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            manager.CreateCommands("", g =>
            {
                //NSFW STUFF!!!
                g.CreateCommand("hentai")
                    .Description($"Shows a random NSFW hentai image from gelbooru and danbooru with a given tag. Tag is optional but preffered. (multiple tags are appended with +) | `-hentai yuri+kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";

                        var links = await Task.WhenAll(SearchHelper.GetGelbooruImageLink("rating%3Aexplicit+" + tag), SearchHelper.GetDanbooruImageLink("rating%3Aexplicit+" + tag)).ConfigureAwait(false);

                        if (links.All(l => l == null))
                        {
                            await e.Channel.SendMessage("`No results.`");
                            return;
                        }

                        await e.Channel.SendMessage(String.Join("\n\n", links)).ConfigureAwait(false);
                    });
                g.CreateCommand("danbooru")
                    .Description($"Shows a random hentai image from danbooru with a given tag. Tag is optional but preffered. (multiple tags are appended with +) | `-danbooru yuri+kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";
                        var link = await SearchHelper.GetDanbooruImageLink(tag).ConfigureAwait(false);
                        if (string.IsNullOrWhiteSpace(link))
                            await e.Channel.SendMessage("Search yielded no results ;(");
                        else
                            await e.Channel.SendMessage(link).ConfigureAwait(false);
                    });
                g.CreateCommand("gelbooru")
                    .Description($"Shows a random hentai image from gelbooru with a given tag. Tag is optional but preffered. (multiple tags are appended with +) | `-gelbooru yuri+kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";
                        var link = await SearchHelper.GetGelbooruImageLink(tag).ConfigureAwait(false);
                        if (string.IsNullOrWhiteSpace(link))
                            await e.Channel.SendMessage("Search yielded no results ;(");
                        else
                            await e.Channel.SendMessage(link).ConfigureAwait(false);
                    });

                g.CreateCommand("rule34")
                    .Description($"Shows a random image from rule34.xx with a given tag. Tag is optional but preffered. (multiple tags are appended with +) | `-rule34 yuri+kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";
                        var link = await SearchHelper.GetRule34ImageLink(tag).ConfigureAwait(false);
                        if (string.IsNullOrWhiteSpace(link))
                            await e.Channel.SendMessage("Search yielded no results ;(");
                        else
                            await e.Channel.SendMessage(link).ConfigureAwait(false);
                    });
                g.CreateCommand("e621")
                    .Description($"Shows a random hentai image from e621.net with a given tag. Tag is optional but preffered. Use spaces for multiple tags. | `-e621 yuri kissing`")
                    .Parameter("tag", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var tag = e.GetArg("tag")?.Trim() ?? "";
                        await e.Channel.SendMessage(await SearchHelper.GetE621ImageLink(tag).ConfigureAwait(false)).ConfigureAwait(false);
                    });
                g.CreateCommand("cp")
                    .Description($"We all know where this will lead you to. | `-cp`")
                    .Parameter("anything", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        await e.Channel.SendMessage("http://i.imgur.com/MZkY1md.jpg").ConfigureAwait(false);
                    });
                g.CreateCommand("boobs")
                    .Description($"Real adult content. | `-boobs`")
                    .Do(async e =>
                    {
                        try
                        {
                            var obj = JArray.Parse(await SearchHelper.GetResponseStringAsync($"http://api.oboobs.ru/boobs/{rng.Next(0, 9380)}").ConfigureAwait(false))[0];
                            await e.Channel.SendMessage($"http://media.oboobs.ru/{ obj["preview"].ToString() }").ConfigureAwait(false);
                        }
                        catch (Exception ex)
                        {
                            await e.Channel.SendMessage($"💢 {ex.Message}").ConfigureAwait(false);
                        }
                    });
                g.CreateCommand("butts")
                    .Alias("ass", "butt")
                    .Description($"Real adult content. | `-butts` or `-ass`")
                    .Do(async e =>
                    {
                        try
                        {
                            var obj = JArray.Parse(await SearchHelper.GetResponseStringAsync($"http://api.obutts.ru/butts/{rng.Next(0, 3373)}").ConfigureAwait(false))[0];
                            await e.Channel.SendMessage($"http://media.obutts.ru/{ obj["preview"].ToString() }").ConfigureAwait(false);
                        }
                        catch (Exception ex)
                        {
                            await e.Channel.SendMessage($"💢 {ex.Message}").ConfigureAwait(false);
                        }
                    });
            });
        }
    }
}
