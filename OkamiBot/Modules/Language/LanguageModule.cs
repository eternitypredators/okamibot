﻿using Discord;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OkamiBot.Modules
{
    public class LanguageModule : IModule
    {
        private DiscordClient _client;
        public void Install(ModuleManager manager)
        {
            _client = manager.Client;

            manager.CreateCommands("", g => {
                g.CreateCommand("jp#hiragana")
                .Alias(new string[] { "jp#hira" })
                .Description("Sends a hiragana chart through Direct Messages (DMs).")
                .Do(async e =>
                {
                    await e.User.SendMessage("https://sthnihongo.files.wordpress.com/2014/07/hiragana-2.jpg");
                });

                g.CreateCommand("jp#katakana")
                    .Alias(new string[] { "jp#kata" })
                    .Description("Sends a katakana chart through Direct Messages (DMs).")
                    .Do(async e =>
                    {
                        await e.User.SendMessage("https://japanesejourneylessons.files.wordpress.com/2014/02/katakana.jpg");
                    });

                g.CreateCommand("jp#hirakataquiz")
                    .Alias(new string[] { "jp#flashcard" })
                    .Description("Sends a message to hiraganaquiz.com.")
                    .Do(async e =>
                    {
                        await _client.Reply(e, "Here is the link you have requested for!~ http://www.hiraganaquiz.com/");
                    });

                g.CreateCommand("krhangul#refchart")
                    .Alias("krhgl#ref")
                    .Description("Sends an image of the Korean Hangul combinations and their consenants + vowels in Direct Messages (DM).")
                    .Do(async e =>
                    {
                        await e.User.SendMessage("https://www.joop.in/wp-content/uploads/2008/09/111.jpg");
                        await e.User.SendMessage("https://www.joop.in/wp-content/uploads/2008/09/21-1024x724.jpg");
                        await e.User.SendMessage("https://www.joop.in/wp-content/uploads/2008/09/31-1024x724.jpg");
                    });

                g.CreateCommand("krhangul#ezlearn")
                    .Alias("krhgl#ezlearn")
                    .Description("Sends an image of an easy way to learn Korean Hangul in Direct Messages (DM).")
                    .Do(async e =>
                    {
                        await e.User.SendMessage("http://9gag.com/gag/3968335/learn-to-read-korean-in-15-minutes");
                    });
            });
        }
    }
}