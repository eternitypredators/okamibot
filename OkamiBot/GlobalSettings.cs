﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

namespace OkamiBot
{
    public class GlobalSettings
    {
        private const string path = "./config/global.json";
        private static GlobalSettings _instance = new GlobalSettings();

        public static void Load()
        {
            if (!File.Exists(path))
                throw new FileNotFoundException($"{path} is missing.");
            _instance = JsonConvert.DeserializeObject<GlobalSettings>(File.ReadAllText(path));

        }
        public static void Save()
        {
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
            using (var writer = new StreamWriter(stream))
                writer.Write(JsonConvert.SerializeObject(_instance, Formatting.Indented));
        }

        //Discord
        public class DiscordSettings
        {
            [JsonProperty("username")]
            public string Email;
            [JsonProperty("password")]
            public string Password;
            [JsonProperty("token")]
            public string Token;
            [JsonProperty("bottoken")]
            public string BotToken;
            [JsonProperty("defaultgame")]
            public string DefaultGame;
        }
        [JsonProperty("discord")]
        private DiscordSettings _discord = new DiscordSettings();
        public static DiscordSettings Discord => _instance._discord;

        //Users
        public class UserSettings
        {
            [JsonProperty("dev")]
            public ulong DevId;
            [JsonProperty("name")]
            public string Name;
            [JsonProperty("tag")]
            public ulong Tag;
            [JsonProperty("server")]
            public string Server;
        }
        [JsonProperty("users")]
        private UserSettings _users = new UserSettings();
        public static UserSettings Users => _instance._users;


        public class osuApiSettings
        {
            [JsonProperty("osu_api_key")]
            public string Osu_Api_Key;
        }
        [JsonProperty("osu")]
        private osuApiSettings _osuApi = new osuApiSettings();
        public static osuApiSettings osuApi => _instance._osuApi;

        public class RiotApiSettings
        {
            [JsonProperty("riot_api_key")]
            public string Riot_Api_Key;
        }
        [JsonProperty("riot")]
        private RiotApiSettings _riotApi = new RiotApiSettings();
        public static RiotApiSettings riotApi => _instance._riotApi;

        public class GoogleApiSettings
        {
            [JsonProperty("googleapi_key")]
            public string GoogleApi_Key;
        }
        [JsonProperty("googleapi")]
        private GoogleApiSettings _googleApi = new GoogleApiSettings();
        public static GoogleApiSettings googleApi => _instance._googleApi;

        /*//Github
        public class GithubSettings
        {
            [JsonProperty("username")]
            public string Username;
            [JsonProperty("password")]
            public string Password;
            [JsonIgnore]
            public string Token => Convert.ToBase64String(Encoding.ASCII.GetBytes(Username + ":" + Password));
        }
        [JsonProperty("github")]
        private GithubSettings _github = new GithubSettings();
        public static GithubSettings Github => _instance._github;*/
    }
}
