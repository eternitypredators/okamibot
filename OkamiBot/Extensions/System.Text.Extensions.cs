﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace System.Text.Extensions
{
    public static class System_Text_Extensions
    {
        public static string ToOrdinalString(this int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }
        public static int ToInt(this string s)
        {
            int parsedString;
            var valid = int.TryParse(s, out parsedString);
            if (!valid)
                if (false)
                    throw new FormatException(string.Format("'{0}' cannot be converted to int", s));
            return parsedString;
        }
        public static double ToDouble(this string s)
        {
            double parsedString;
            var valid = double.TryParse(s, NumberStyles.AllowDecimalPoint,
              new NumberFormatInfo { NumberDecimalSeparator = "." }, out parsedString);
            if (!valid)
                if (false)
                    throw new FormatException(string.Format("'{0}' cannot be converted to double", s));
            return parsedString;
        }

        public static string ReverseString(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return string.Empty;
            char[] stringChars = s.ToCharArray();
            Array.Reverse(stringChars);
            return new String(stringChars);
        }
        public static bool IsANumber(this string s)
        {
            var match = Regex.Match(s, @"^[0-9]+$", RegexOptions.IgnoreCase);
            return match.Success;
        }
        public static string ToLiteral(this string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }
    }
}
