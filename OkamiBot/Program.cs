using Discord;
using Discord.Audio;
using Discord.Modules;
using Discord.Commands;
using Discord.Commands.Permissions.Levels;
using Discord.Commands.Permissions.Userlist;
using OkamiBot.Modules;
using OkamiBot.Modules.AnimeModule;
using OkamiBot.Modules.NSFWModule;
using OkamiBot.Modules.osu_Module;
using OkamiBot.Modules.StatusModule;
using OkamiBot.Modules.TranslatorModule;
using OkamiBot.Modules.TwitchModule;
using OkamiBot.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace OkamiBot
{
	public class Program
	{
        public static void Main(string[] args) => new Program().Start(args);

        const Int32 SW_MINIMIZE = 6;

		[DllImport("Kernel32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		private static extern IntPtr GetConsoleWindow();

		[DllImport("User32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool ShowWindow([In] IntPtr hWnd, [In] Int32 nCmdShow);

		const int SW_HIDE = 0;
		const int SW_SHOW = 5;

        //Method to hide console window.
		public static void HideConsoleWindow()
		{
			IntPtr hWndConsole = GetConsoleWindow();
			ShowWindow(hWndConsole, SW_HIDE);
		}

        //Method to show console window.
		public static void ShowConsoleWindow()
		{
			IntPtr hWndConsole = GetConsoleWindow();
			ShowWindow(hWndConsole, SW_SHOW);
		}

        private const string AppName = "ShinobuBot";
        private const string AppUrl = "https://discordapp.com/oauth2/authorize?client_id=176148241455382538&scope=bot&permissions=0";

        public static DiscordClient _client;
        private void Start(string[] args)
        {
            //Hide Discord Bot Console Window As Program BEgings
            HideConsoleWindow();

            //Set Title Of Console Window
            Console.Title = $"{AppName} - A Discord Bot (Discord.Net v{DiscordConfig.LibVersion})";

            //Load Global Settings
            GlobalSettings.Load();

            //Initalizing Configuration Of Discord Client
            _client = new DiscordClient(x =>
            {
                x.AppName = AppName;
                x.AppUrl = AppUrl;
                x.MessageCacheSize = 0;
                x.UsePermissionsCache = true;
                x.EnablePreUpdateEvents = true;
                x.LogLevel = LogSeverity.Debug;
                x.LogHandler = OnLogMessage;
                x.FailedReconnectDelay = 750;
            })
            .UsingCommands(x =>
            {
                x.AllowMentionPrefix = true;
                x.PrefixChar = '-';
                x.HelpMode = HelpMode.Disabled;
                x.ExecuteHandler = OnCommandExecuted;
                x.ErrorHandler = OnCommandError;
            })
            .UsingModules()
            .UsingAudio(x =>
            {
                x.Mode = AudioMode.Outgoing;
                x.EnableEncryption = true;
                x.Bitrate = AudioServiceConfig.MaxBitrate;
                x.BufferLength = 10000;
                x.Channels = 2;
            })
            .UsingPermissionLevels(PermissionResolver);

            //Adding Discord Bot Client Services
            var data = _client.AddService<DataService>();
            _client.AddService<SettingsService>();
            _client.AddService<HttpService>();

            //Adding Command Modules
            _client.AddModule<ModulesModule>("Modules-Control", ModuleFilter.None);
            _client.AddModule<OwnerModule>("Owner-Commands", ModuleFilter.None);
            _client.AddModule<BotCommandModule>("Bot-Related-Commands", ModuleFilter.None);
            _client.AddModule<HelpModule>("Help-Commands", ModuleFilter.None);
            _client.AddModule<ChannelModule>("Channel-Moderation", ModuleFilter.None);
            _client.AddModule<AudioModule>("Audio-Module", ModuleFilter.None);
            _client.AddModule<ServerModeratorModule>("Server-Moderation", ModuleFilter.None);
            _client.AddModule<ServerStatsModule>("Server-Statistics", ModuleFilter.None);
            _client.AddModule<osu_Module>("osu!-Module", ModuleFilter.None);
            _client.AddModule<LanguageModule>("Languages-Help", ModuleFilter.None);
            _client.AddModule<MiscellaneousModule>("Miscellaneous", ModuleFilter.None);
            _client.AddModule<StatusModule>("Discord-Status", ModuleFilter.None);
            _client.AddModule<TwitchModule>("Twitch-Streams", ModuleFilter.None);
            _client.AddModule<AnimeModule>("Anime-Search", ModuleFilter.None);
            _client.AddModule<ElswordModule>("Elsword-Module", ModuleFilter.None);
            _client.AddModule<GamblingModule>("Gambling-Module", ModuleFilter.None);
            _client.AddModule<ConversionsModule>("Conversions-Module", ModuleFilter.None);
            _client.AddModule<ComicsModule>("Comics-Module", ModuleFilter.None);
            _client.AddModule<NSFWModule>("NSFW-Module", ModuleFilter.ChannelWhitelist);
            _client.AddModule<TranslatorModule>("Translator-Module", ModuleFilter.None);
            
            //Standard Message For User Joining Servers
            _client.UserJoined += Client_UserJoined;
            //Standard Message For User Leaving A Server
            _client.UserLeft += Client_UserLeft;

            //Logging Messages Sent To A Channel
            _client.MessageReceived += (s, e) => {
                if (!e.Message.IsAuthor) WriteLog(LogSeverity.Info,
                    e.Channel.IsPrivate ? $"[#{e.Channel?.Name}] {e.User?.Name}: {e.Message?.Text}" : $"[{e.Server.Name}/#{e.Channel?.Name}] {e.User?.Name}: {e.Message?.Text}");
            };

            //Logging Messages Deleted From A Channel
            _client.MessageDeleted += (s, e) => {
                if (!e.Message.IsAuthor) WriteLog(LogSeverity.Info,
                    e.Channel.IsPrivate ? $"[#{e.Channel?.Name}] {e.User?.Name}: {e.Message?.Text} [DELETED]" : $"[{e.Server.Name}/#{e.Channel?.Name}] {e.User?.Name}: {e.Message?.Text} [DELETED]");
            };

            //Message Updated Event - Currently Doing Nothing
            _client.MessageUpdated += (s, e) => {

            };

            //Message Received Event - Captures Text And Sends Back A Response To Specific Messages
            _client.MessageReceived += _client_MessageReceived;

            //Ready Event - Used To Set Bot's Name And Current Game It Is Playing
            _client.Ready += _client_Ready;

            //Initialized local variables for Command and Audio services in the case that commands are coded in the main rather than modules.
            var cS = _client.GetService<CommandService>();
            var auServ = _client.GetService<AudioService>();

            //Specifying which type of login to use for the bot.
            _client_ConnectUsingUserAccount();
        }

        private static async void _client_Ready(object sender, EventArgs e)
        {
            if (_client.CurrentUser.Name != _client.Config.AppName)
                await _client.CurrentUser.Edit(GlobalSettings.Discord.Password, $"{_client.Config.AppName}");
            _client.SetGame(GlobalSettings.Discord.DefaultGame);
        }

        public static  void _client_ConnectUsingUserAccount()
        {
            _client.ExecuteAndWait(async () =>
            {
                while (true)
                {
                    try
                    {
                        //Connect to the Discord server using our email and password
                        await _client.Connect(GlobalSettings.Discord.Email, GlobalSettings.Discord.Password);
                        _client.GetService<DataService>().Load();
                        //If we are not a member of any server
                        if (!_client.Servers.Any()) await _client.GetInvite(GlobalSettings.Users.Server).Result.Accept();
                        WriteLog(LogSeverity.Info, "Loading data!");
                        break;
                    }
                    catch (Exception ex)
                    {
                        _client.Log.Error($"Login Failed", ex);
                        await Task.Delay(_client.Config.FailedReconnectDelay);
                    }
                }
            });
        }
        public static void _client_ConnectUsingBotToken()
        {
            _client.ExecuteAndWait(async () =>
            {
                while (true)
                {
                    try
                    {
                        //Connect to the Discord server using our bot token from our developer page
                        await _client.Connect(GlobalSettings.Discord.BotToken, TokenType.Bot);
                        _client.GetService<DataService>().Load();
                        WriteLog(LogSeverity.Info, "Loading data!");
                        break;
                    }
                    catch (Exception ex)
                    {
                        _client.Log.Error($"Login Failed", ex);
                        await Task.Delay(_client.Config.FailedReconnectDelay);
                    }
                }
            });
        }

        private static async void Client_UserJoined(object sender, UserEventArgs e)
        {
            if (e.Server.Id == 97368228304539648)
            {
                var channel = _client.GetChannel(97368228304539648);
                await channel.SendMessage($"{Format.Bold(e.User.NicknameMention)} joined {Format.Bold(e.Server.Name)}.\nWelcome to the server~ Please enjoy your stay!~ :smile:");
            }
        }

        private static async void Client_UserLeft(object sender, UserEventArgs e)
        {
            if (e.Server.Id == 97368228304539648)
            {
                var channel = _client.GetChannel(97368228304539648);
                await channel.SendMessage($"{Format.Bold(e.User.NicknameMention)} has left {Format.Bold(e.Server.Name)}! Bye bye!!~");
            }
        }

        private static async void _client_MessageReceived(object sender, MessageEventArgs e)
        {
            string msg = e.Message.Text;
				if (!e.Message.IsAuthor)
				{
                    if (msg.ToLower().Contains("kaka") || msg.Contains("かか") 
                        || msg.Contains("カカ") || msg.Contains("ｶｶ"))
                        await e.Channel.SendMessage($"{Format.Italics("Kaka")}");
                    else if (msg.ToLower().Contains("puchi") || msg.ToLower().Contains("ぷち")
                        || msg.Contains("プチ") || msg.Contains("ﾌﾟﾁ"))
                        await e.Channel.SendMessage($"{Format.Bold("Tomato!")}");
					//switch (msg.ToLower())
					//{
						//case "kek":
							//await e.Channel.SendMessage(Format.Italics("kek"));
							//break;
						//case "meme":
							//await e.Channel.SendMessage(Format.Bold("SEAN IS THE ULTIMATE MEMELORD. (You need not know who this is!) :trumpet: :trumpet: :trumpet: :trumpet: :trumpet: :trumpet: :trumpet:"));
							//break;
						//case "spooky":
						//case "creepy":
							//await e.Channel.SendMessage(":skull: :trumpet: :skull: :trumpet: :skull: :trumpet: :skull: :trumpet: :skull: :trumpet: ");
							//break;
						//case "ayy":
							//await e.Channel.SendMessage("░░░░█▒▒▄▀▀▀▀▀▄▄▒▒▒▒▒▒▒▒▒▄▄▀▀▀▀▀▀▄\n░░▄▀▒▒▒▄█████▄▒█▒▒▒▒▒▒▒█▒▄█████▄▒█\n░█▒▒▒▒▐██▄████▌▒█▒▒▒▒▒█▒▐██▄████▌▒█\n▀▒▒▒▒▒▒▀█████▀▒▒█▒░▄▒▄█▒▒▀█████▀▒▒▒█\n▒▒▐▒▒▒░░░░▒▒▒▒▒█▒░▒▒▀▒▒█▒▒▒▒▒▒▒▒▒▒▒▒█\n▒▌▒▒▒░░░▒▒▒▒▒▄▀▒░▒▄█▄█▄▒▀▄▒▒▒▒▒▒▒▒▒▒▒▌\n▒▌▒▒▒▒░▒▒▒▒▒▒▀▄▒▒█▌▌▌▌▌█▄▀▒▒▒▒▒▒▒▒▒▒▒▐\n▒▐▒▒▒▒▒▒▒▒▒▒▒▒▒▌▒▒▀███▀▒▌▒▒▒▒▒▒▒▒▒▒▒▒▌\n▀▀▄▒▒▒▒▒▒▒▒▒▒▒▌▒▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▒▒█\n▀▄▒▀▄▒▒▒▒▒▒▒▒▐▒▒▒▒▒▒▒▒▒▄▄▄▄▒▒▒▒▒▒▄▄▀\n▒▒▀▄▒▀▄▀▀▀▄▀▀▀▀▄▄▄▄▄▄▄▀░░░░▀▀▀▀▀▀\n▒▒▒▒▀▄▐▒▒▒▒▒▒▒▒▒▒▒▒▒▐\n░▄▄▄░░▄░░▄░▄░░▄░░▄░░░░▄▄░▄▄░░░▄▄▄░░░▄▄▄\n█▄▄▄█░█▄▄█░█▄▄█░░█░░░█░░█░░█░█▄▄▄█░█░░░█\n█░░░█░░█░░░░█░░░░█░░░█░░█░░█░█░░░█░█░░░█\n▀░░░▀░░▀░░░░▀░░░░▀▀▀░░░░░░░░░▀░░░▀░▀▄▄▄▀");
							//break;
						//case "kys okami!":
							//await e.Channel.SendMessage("Am I not appreciated here?! Hmph! I'll be back again someday!!!! And even more powerful!");
							//Environment.Exit(0);
							//break;
						//case "(╯°□°）╯︵ ┻━┻":
							//await e.Channel.SendMessage($"{e.User.Mention} DO YOU DARE DEFY ME?!?! DON'T FLIP MY SACRED TABLE!!!\n┬─┬﻿ ノ( ゜-゜ノ)"); //Message does not send before exit... Why?..
                            //break;
						//case @"im sorry":
						//case @"I'm sorry":
						//case @"I apologize":
						//case @"sry":
						//case @"sorry": //Work on this later.
							//await e.Channel.SendMessage($"{e.User.Mention}That's right, show some praise and respect.");
							//break;
						//case "ping":
							//await e.Channel.SendMessage($"{e.User.Mention} pong!");
							//break;
					//}
				}
        }

        private void OnCommandError(object sender, CommandErrorEventArgs e)
        {
            string msg = e.Exception?.Message;
            if (msg == null) //No exception - show a generic message
            {
                switch (e.ErrorType)
                {
                    case CommandErrorType.Exception:
                        msg = "Unknown error.";
                        break;
                    case CommandErrorType.BadPermissions:
                        msg = "You do not have permission to run this command.";
                        break;
                    case CommandErrorType.BadArgCount:
                        msg = "You provided the incorrect number of arguments for this command.";
                        break;
                    case CommandErrorType.InvalidInput:
                        msg = "Unable to parse your command, please check your input.";
                        break;
                    /*case CommandErrorType.UnknownCommand:
                        msg = "Unknown command.";
                        break; Not catching unknown commands due to people's chat habits
                        */
                }
            }
            if (msg != null)
            {
                _client.ReplyError(e, msg);
                _client.Log.Error("Command", msg);
            }
        }

        private void OnCommandExecuted(object sender, CommandEventArgs e)
        {
            _client.Log.Info("Command", $"{e.Command.Text} ({e.User.Name})");
        }

        private void OnLogMessage(object sender, LogMessageEventArgs e)
        {
            //Color
            ConsoleColor color;
            switch (e.Severity)
            {
                case LogSeverity.Error: color = ConsoleColor.Red; break;
                case LogSeverity.Warning: color = ConsoleColor.Yellow; break;
                case LogSeverity.Info: color = ConsoleColor.White; break;
                case LogSeverity.Verbose: color = ConsoleColor.Gray; break;
                case LogSeverity.Debug: default: color = ConsoleColor.DarkGray; break;
            }

            //Exception
            string exMessage;
            Exception ex = e.Exception;
            if (ex != null)
            {
                while (ex is AggregateException && ex.InnerException != null)
                    ex = ex.InnerException;
                exMessage = ex.Message;
            }
            else
                exMessage = null;

            //Source
            string sourceName = e.Source?.ToString();

            //Text
            string text;
            if (e.Message == null)
            {
                text = exMessage ?? "";
                exMessage = null;
            }
            else
                text = e.Message;

            //Build message
            StringBuilder builder = new StringBuilder(text.Length + (sourceName?.Length ?? 0) + (exMessage?.Length ?? 0) + 5);
            if (sourceName != null)
            {
                builder.Append('[');
                builder.Append(sourceName);
                builder.Append("] ");
            }
            for (int i = 0; i < text.Length; i++)
            {
                //Strip control chars
                char c = text[i];
                if (!char.IsControl(c))
                    builder.Append(c);
            }
            if (exMessage != null)
            {
                builder.Append(": ");
                builder.Append(exMessage);
            }

            text = builder.ToString();
            Console.ForegroundColor = color;
            Console.WriteLine(text);
        }

        private int PermissionResolver(User user, Channel channel)
        {
            if (user.Id == GlobalSettings.Users.DevId)
                return (int)PermissionLevel.BotOwner;
            if (user.Server != null)
            {
                if (user == channel.Server.Owner)
                    return (int)PermissionLevel.ServerOwner;

                var serverPerms = user.ServerPermissions;
                if (serverPerms.ManageRoles)
                    return (int)PermissionLevel.ServerAdmin;
                if (serverPerms.ManageMessages && serverPerms.KickMembers && serverPerms.BanMembers)
                    return (int)PermissionLevel.ServerModerator;

                var channelPerms = user.GetPermissions(channel);
                if (channelPerms.ManagePermissions)
                    return (int)PermissionLevel.ChannelAdmin;
                if (channelPerms.ManageMessages)
                    return (int)PermissionLevel.ChannelModerator;
            }
            return (int)PermissionLevel.User;
        }

        public static void WriteLog(LogSeverity s, string text)
        {
            ConsoleColor color;
            switch (s)
            {
                case LogSeverity.Error: color = ConsoleColor.Red; break;
                case LogSeverity.Warning: color = ConsoleColor.Yellow; break;
                case LogSeverity.Info: color = ConsoleColor.White; break;
                case LogSeverity.Verbose: color = ConsoleColor.Gray; break;
                case LogSeverity.Debug: default: color = ConsoleColor.DarkGray; break;
            }
            Console.ForegroundColor = color;
            Console.WriteLine("[{0}]: {1}", DateTime.Now.ToLongTimeString(), text);
        }
    }
}