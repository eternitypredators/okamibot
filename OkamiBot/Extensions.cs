using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Legacy;
using Newtonsoft.Json;
using OkamiBot;
public static class Extensions
{
    public static Task Reply(this DiscordClient client, CommandEventArgs e, string text)
        => Reply(client, e.User, e.Channel, text);
    public static async Task Reply(this DiscordClient client, User user, Channel channel, string text)
    {
        if (text != null)
        {
            if (!channel.IsPrivate)
                await channel.SendMessage($"{user.Mention} {text}");
            else
                await user.SendMessage(text);
        }
    }
    public static Task Reply<T>(this DiscordClient client, CommandEventArgs e, string prefix, T obj)
        => Reply(client, e.User, e.Channel, prefix, obj != null ? JsonConvert.SerializeObject(obj, Formatting.Indented) : "null");
    public static Task Reply<T>(this DiscordClient client, User user, Channel channel, string prefix, T obj)
        => Reply(client, user, channel, prefix, obj != null ? JsonConvert.SerializeObject(obj, Formatting.Indented) : "null");
    public static Task Reply(this DiscordClient client, CommandEventArgs e, string prefix, string text)
        => Reply(client, e.User, e.Channel, (prefix != null ? $"{Format.Bold(prefix)}:\n" : "\n") + text);
    public static Task Reply(this DiscordClient client, User user, Channel channel, string prefix, string text)
        => Reply(client, user, channel, (prefix != null ? $"{Format.Bold(prefix)}:\n" : "\n") + text);

    public static Task ReplyError(this DiscordClient client, CommandEventArgs e, string text)
        => Reply(client, e.User, e.Channel, "Error: " + text);
    public static Task ReplyError(this DiscordClient client, User user, Channel channel, string text)
        => Reply(client, user, channel, "Error: " + text);
    public static Task ReplyError(this DiscordClient client, CommandEventArgs e, Exception ex)
        => Reply(client, e.User, e.Channel, "Error: " + ex.GetBaseException().Message);
    public static Task ReplyError(this DiscordClient client, User user, Channel channel, Exception ex)
        => Reply(client, user, channel, "Error: " + ex.GetBaseException().Message);

    /*public static async Task<Message> Send(this CommandEventArgs e, string message) => await e.Channel.SendMessage(message);
    public static async Task Send(this MessageEventArgs e, string message) => await e.Channel.SendMessage(message);
    public static async Task Send(this Channel c, string message) => await c.SendMessage(message);
    public static async Task Send(this User u, string message) => await u.SendMessage(message);*/

    public static void Log(this DiscordClient client, LogMessageEventArgs e)
        => Log(client, e.Severity, e.Source, e.Message, e.Exception);
    public static void Log(this DiscordClient client, LogSeverity severity, string text, Exception ex = null)
        => Log(client, severity, null, text, ex);
    public static void Log(this DiscordClient client, LogSeverity severity, object source, string text, Exception ex = null)
    {
        char severityChar;
        ConsoleColor color;
        switch (severity)
        {
            case LogSeverity.Error:
                severityChar = 'E';
                color = ConsoleColor.Red;
                break;
            case LogSeverity.Warning:
                severityChar = 'W';
                color = ConsoleColor.Yellow;
                break;
            case LogSeverity.Info:
                severityChar = 'I';
                color = ConsoleColor.White;
                break;
            case LogSeverity.Verbose:
                severityChar = 'V';
                color = ConsoleColor.Gray;
                break;
            case LogSeverity.Debug:
                severityChar = 'D';
                color = ConsoleColor.DarkGray;
                break;
            default:
                severityChar = '?';
                color = ConsoleColor.Gray;
                break;
        }

        if (source != null)
            text = $"[{source}] {text}";
        if (ex != null)
            text = $"{text}: {ex.GetBaseException().Message}";
        if (severity <= LogSeverity.Info || (source is string))
        {
            Console.ForegroundColor = color;
            Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] {text}");
        }

        text = $"[{DateTime.Now.ToLongTimeString()}] {severityChar} {text}";
        Debug.WriteLine(text);
    }
}

internal static class InternalExtensions
{
    public static Task<User[]> FindUsers(this DiscordClient client, CommandEventArgs e, string username, string discriminator)
        => FindUsers(client, e, username, discriminator, false);
    public static async Task<User> FindUser(this DiscordClient client, CommandEventArgs e, string username, string discriminator)
        => (await FindUsers(client, e, username, discriminator, true))?[0];
    public static async Task<User[]> FindUsers(this DiscordClient client, CommandEventArgs e, string username, string discriminator, bool singleTarget)
    {
        IEnumerable<User> users;
        if (discriminator == "")
            users = e.Server.FindUsers(username);
        else
        {
            //User user = client.GetUser(e.Server, username, ushort.Parse(discriminator));
            var user = e.Server.GetUser(username, ushort.Parse(discriminator));
            users = user == null ? Enumerable.Empty<User>() : new User[] { user };
        }

        var count = users.Count();
        if (singleTarget)
        {
            if (count == 0)
            {
                await client.ReplyError(e, "User was not found.");
                return null;
            }
            else if (count > 1)
            {
                await client.ReplyError(e, "Multiple users were found with that username.");
                return null;
            }
        }
        else
        {
            if (count == 0) await client.ReplyError(e, "No user was found.");
        }
        return users.ToArray();
    }

    public static async Task<User> GetUser(this DiscordClient client, CommandEventArgs e, ulong userId)
    {
        var user = e.Server.GetUser(userId);

        if (user == null)
        {
            await client.ReplyError(e, "No user was not found.");
        }
        return user;
    }

    public static async Task<Channel> FindChannel(this DiscordClient client, CommandEventArgs e, string name, ChannelType type = null)
    {
        var channels = e.Server.FindChannels(name, type);

        var count = channels.Count();
        if (count == 0)
        {
            await client.ReplyError(e, "Channel was not found.");
            return null;
        }
        else if (count > 1)
        {
            await client.ReplyError(e, "Multiple channels were found with that name.");
            return null;
        }
        return channels.FirstOrDefault();
    }
}